package main

import (
	"fmt"
	"os"

	aliyun "gitee.com/qqjtxc/project/aliyun/apps"
	_ "gitee.com/qqjtxc/project/aliyun/apps/all"
	"github.com/gin-gonic/gin"
)

func main() {
	if err := aliyun.InitApp(); err != nil {
		panic(err)
	}

	server := gin.Default()
	v1 := server.Group("/aliyun")
	if err := aliyun.InitHttpApps(v1); err != nil {
		fmt.Println(err)
		os.Exit(2)
	}

	// 	启动HTTP接口监听
	if err := server.Run(":8000"); err != nil {
		fmt.Println(err)
	}
}
