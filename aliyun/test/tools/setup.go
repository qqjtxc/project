package tools

import (
	aliyun "gitee.com/qqjtxc/project/aliyun/apps"
)

// 开发环境 单元测试的Setup
func DevelopmentSet() {
	// err := config.LoadConfigFromToml(config.EtcFilePath)
	// if err != nil {
	// 	panic(err)
	// }

	//初始化测试类的注册
	if err := aliyun.InitApp(); err != nil {
		panic(err)
	}
}
