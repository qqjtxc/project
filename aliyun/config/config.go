package config

var (
	EtcFilePath = "/Users/xuchi/go/project/project/aliyun/etc/aliyun.toml"
)

// 保存项目所有配置信息
type Config struct {
	AliYunMsg *AliYunMsg `toml:"aliyun"`
}

func NewConfig() *Config {
	return &Config{
		AliYunMsg: NewAliYunMsg(),
	}
}

// toml中针对阿里云对象设置
type AliYunMsg struct {
	AccessKeyId     string `toml:"accessKeyId"`
	AccessKeySecret string `toml:"accessKeySecret"`
	Endpoint        string `toml:"endpoint"`
	WafEndpoint     string `toml:"wafEndpoint"`
}

func NewAliYunMsg() *AliYunMsg {
	return &AliYunMsg{}
}
