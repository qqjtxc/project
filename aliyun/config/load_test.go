package config_test

import (
	"testing"

	"gitee.com/qqjtxc/project/aliyun/config"
)

func TestLoadConfigFromToml(t *testing.T) {
	err := config.LoadConfigFromToml(config.EtcFilePath)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(config.ChangeConfig().AliYunMsg)
}
