package tools

import (
	"fmt"
	"reflect"
)

const (
	toolResp = -1
)

func StringChangeInt(ins any) error {
	// if str != "" {
	// 	strInt, err := strconv.Atoi(str)
	// 	if err != nil {
	// 		return strInt, nil
	// 	}
	// 	return toolResp, err
	// }
	// return toolResp, fmt.Errorf("%s 解析失败", str)
	// retu

	typeOfCat := reflect.TypeOf(ins)
	// 遍历结构体所有成员
	for i := 0; i < typeOfCat.NumField(); i++ {
		// 获取每个成员的结构体字段类型
		fieldType := typeOfCat.Field(i)
		// 输出成员名和tag
		fmt.Printf("name: %v  tag: '%v'\n", fieldType.Name, fieldType.Tag)
	}
	return nil
}
