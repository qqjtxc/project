package client

import (
	"gitee.com/qqjtxc/project/aliyun/config"
	openapi "github.com/alibabacloud-go/darabonba-openapi/v2/client"
	"github.com/alibabacloud-go/tea/tea"
)

// 声明一个全局的 openapi.Config
var (
	AliConf = &openapi.Config{}
	ConfTmp = &config.Config{}
)

func init() {
	// 加载配置
	err := config.LoadConfigFromToml(config.EtcFilePath)
	if err != nil {
		panic(err)
	}
	ConfTmp = config.ChangeConfig()
	// 初始化配置
	AliConf = &openapi.Config{
		// 必填，您的 AccessKey ID
		AccessKeyId: tea.String(ConfTmp.AliYunMsg.AccessKeyId),
		// 必填，您的 AccessKey Secret
		AccessKeySecret: tea.String(ConfTmp.AliYunMsg.AccessKeySecret),
	}
}
