package wafcli_test

import (
	"testing"

	"gitee.com/qqjtxc/project/aliyun/apps/client/wafcli"
)

func TestGetWaFClient(t *testing.T) {
	cli, err := wafcli.GetWaFClient()
	if err != nil {
		t.Fatal(err)
	}
	t.Log(cli.Cli)
}
