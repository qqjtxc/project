package wafcli

import (
	"gitee.com/qqjtxc/project/aliyun/apps/client"
	"gitee.com/qqjtxc/project/aliyun/logger"
	"github.com/alibabacloud-go/tea/tea"
	waf_openapi20190910 "github.com/alibabacloud-go/waf-openapi-20190910/v2/client"
)

const (
	AppName = "wafcli"
)

type WafClient struct {
	Cli *waf_openapi20190910.Client
}

func GetWaFClient() (*WafClient, error) {
	_result, _err := wafCreateClient()
	if _err != nil {
		logger.L().Info().Msgf("WAF 创建客户端连接失败:%s", _err)
		return nil, _err
	}
	cli := &WafClient{}
	cli.Cli = _result
	return cli, nil

}

// 初始化一个WAF连接
func wafCreateClient() (_result *waf_openapi20190910.Client, _err error) {
	// 访问的域名
	client.AliConf.Endpoint = tea.String(client.ConfTmp.AliYunMsg.WafEndpoint)

	_result = &waf_openapi20190910.Client{}
	_result, _err = waf_openapi20190910.NewClient(client.AliConf)
	return _result, _err
}

// func (i *impl) Name() string {
// 	return AppName
// }

// // 对实例进行初始化
// func (i *impl) Init() error {
// 	// 通过ioc完成注册
// 	return nil
// }

// func init() {
// 	_result, err := WafCreateClient()
// 	if err != nil {
// 		return
// 	}
// 	// 把客户端初始化进去
// 	imp := &impl{}
// 	imp.client = _result
// 	aliyun.RegistryClient(imp)
// }
