package ecscli

import (
	"gitee.com/qqjtxc/project/aliyun/apps/client"
	"gitee.com/qqjtxc/project/aliyun/logger"
	ecs20140526 "github.com/alibabacloud-go/ecs-20140526/v3/client"
	"github.com/alibabacloud-go/tea/tea"
)

const (
	AppName = "ecscli"
)

type EcsClient struct {
	Cli *ecs20140526.Client
}

// 获取一个ECS连接
func GetEcsClient() (*EcsClient, error) {
	_result, _err := ecsCreateClient()
	if _err != nil {
		logger.L().Info().Msgf("ECS 创建客户端连接失败:%s", _err)
		return nil, _err
	}
	cli := &EcsClient{}
	cli.Cli = _result
	return cli, nil
}

// 初始化一个ECS连接
func ecsCreateClient() (_result *ecs20140526.Client, _err error) {

	// 访问的域名
	client.AliConf.Endpoint = tea.String(client.ConfTmp.AliYunMsg.Endpoint)

	_result = &ecs20140526.Client{}
	_result, _err = ecs20140526.NewClient(client.AliConf)
	return _result, _err
}
