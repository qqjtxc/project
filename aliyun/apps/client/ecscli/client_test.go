package ecscli_test

import (
	"testing"

	"gitee.com/qqjtxc/project/aliyun/apps/client/ecscli"
)

func TestGetWaFClient(t *testing.T) {
	cli, err := ecscli.GetEcsClient()
	if err != nil {
		t.Fatal(err)
	}
	t.Log(cli.Cli)
}
