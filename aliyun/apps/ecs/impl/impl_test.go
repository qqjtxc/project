package impl_test

import (
	"context"

	"gitee.com/qqjtxc/project/aliyun/apps"
	"gitee.com/qqjtxc/project/aliyun/apps/ecs"
	"gitee.com/qqjtxc/project/aliyun/test/tools"
	util "github.com/alibabacloud-go/tea-utils/v2/service"
)

var (
	svc     ecs.Service
	runtime = &util.RuntimeOptions{}
	ctx     = context.Background()
)

func init() {
	// 加载测试用例的配置对象
	tools.DevelopmentSet()
	// 从ioc中获取对应的ecs service的具体实现
	svc = apps.GetInternalApp(ecs.AppName).(ecs.Service)
}

func Name() string {
	return ecs.AppName
}
