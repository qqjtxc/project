package impl

import (
	"context"
	"encoding/json"
	"strings"
	"time"

	"gitee.com/qqjtxc/project/aliyun/apps/ecs"
	"gitee.com/qqjtxc/project/aliyun/logger"
	util "github.com/alibabacloud-go/tea-utils/v2/service"
)

func (i *impl) SelectAllEcs(ctx context.Context, req *ecs.SelectAllEcsRequest, runtime *util.RuntimeOptions) (
	*ecs.SelectAllEcsResponse, error) {
	result := ecs.NewSelectAllEcsResponse()
	_result, err := i.Client.Cli.DescribeInstanceStatusWithOptions(req.DescribeInstanceStatusRequest, runtime)
	if err != nil {
		logger.L().Debug().Msgf(err.Error())
		return nil, err
	}
	result.DescribeInstanceStatusResponse = _result
	return result, nil
}

func (i *impl) QueryEcsDescribeInstances(ctx context.Context, req *ecs.QueryEcsDescribeInstancesRequest, runtime *util.RuntimeOptions) (
	*ecs.QueryEcsDescribeInstancesResponse, error) {
	result := &ecs.QueryEcsDescribeInstancesResponse{}
	// 判断请求中是否获取指定的ECS实例信息，（支持 内网IP 或 InstanceId）
	// 最多支持50台 ECS同时查询
	if (req.DescribeInstancesRequest.InstanceIds == nil || *req.DescribeInstancesRequest.InstanceIds == "") &&
		(req.DescribeInstancesRequest.PrivateIpAddresses == nil || *req.DescribeInstancesRequest.PrivateIpAddresses == "") {
		// 当在请求中没有传获取指定ECS的实例信息，默认查询前10个ECS信息
		instanceReq := ecs.NewSelectAllEcsRequest()
		instanceReq.DescribeInstanceStatusRequest.PageNumber = req.DescribeInstancesRequest.PageNumber
		instanceReq.DescribeInstanceStatusRequest.PageSize = req.DescribeInstancesRequest.PageSize
		_resout, err := i.SelectAllEcs(ctx, instanceReq, runtime)
		if err != nil {
			logger.L().Debug().Msgf(err.Error())
			return nil, err
		}
		// 请求结果组合成一个实例ID列表
		insList := []string{}
		for _, value := range _resout.DescribeInstanceStatusResponse.Body.InstanceStatuses.InstanceStatus {
			insList = append(insList, *value.InstanceId)
		}
		jsonList, _ := json.Marshal(insList)

		// 获取的实例结果填入请求实例详情请求数组中
		req.DescribeInstancesRequest.SetInstanceIds(string(jsonList))
	}

	// 查询实例信息
	needRsult, err := i.Client.Cli.DescribeInstancesWithOptions(req.DescribeInstancesRequest, runtime)
	if err != nil {
		logger.L().Debug().Msgf(err.Error())
		return nil, err
	}

	// 封装自己需要的实例信息格式
	tmpIns := ecs.EcsDescribe{}
	for _, value := range needRsult.Body.Instances.Instance {
		tmpIns.InstanceName = *value.InstanceName
		// 判断ecs实例申请到期时间
		tmpInsName := strings.Split(tmpIns.InstanceName, "-")
		if len(tmpInsName) == 3 && len(tmpInsName[2]) == 8 {
			timeNow := time.Now()
			timeEcs, _ := time.Parse("20060102", tmpInsName[2])
			sub := int32(timeEcs.Sub(timeNow).Hours())
			tmpIns.EcsExpiretime = (sub + 48) / 24
		} else {
			continue
		}

		tmpIns.Cpu = *value.Cpu
		tmpIns.Memory = *value.Memory
		tmpIns.InstanceId = *value.InstanceId
		result.Ecs = append(result.Ecs, tmpIns)
	}

	result.Total = int32(len(result.Ecs))
	return result, nil
}

func (i *impl) QueryPriceEcs(ctx context.Context, req *ecs.QueryPriceEcsRequest, runtime *util.RuntimeOptions) (
	*ecs.QueryPriceEcsResponse, error) {
	priceResult := ecs.NewQueryPriceEcsResponse()
	specesReq := ecs.NewQuerySpecsZoneEcsRequest()
	// 封装规格查询的请求
	// 设置cpu大小
	specesReq.DescribeAvailableResourceRequest.SetCores(req.Cpus)
	// 设置内存大小
	specesReq.DescribeAvailableResourceRequest.SetMemory(req.MemSize)

	// 先进行规格信息查询，获取规格后才可以获取规格对应的价格
	_specesReq, err := i.QuerySpecsZoneEcs(ctx, specesReq)
	if err != nil {
		logger.L().Debug().Msgf(err.Error())
		return nil, err
	}

	//价格查询的请求
	singEcs := ecs.NewSingleEcsPrice()
	for _, v := range _specesReq.SpecsZoneEcs {
		req.DescribePriceRequest.SetInstanceType(*v)

		_result, err := i.Client.Cli.DescribePriceWithOptions(req.DescribePriceRequest, runtime)
		if err != nil {
			logger.L().Debug().Msgf(err.Error())
			continue
		}
		singEcs.SpecsEcs = *v
		singEcs.PriceEcs = *_result.Body.PriceInfo.Price.TradePrice
		priceResult.PriceEcs = append(priceResult.PriceEcs, *singEcs)
	}
	// 统计总条数
	priceResult.Totals = len(priceResult.PriceEcs)
	return priceResult, nil
}

func (i *impl) QuerySpecsEcs(ctx context.Context, req *ecs.QuerySpecsEcsRequest, runtime *util.RuntimeOptions) (
	*ecs.QuerySpecsEcsResponse, error) {
	result := ecs.NewQuerySpecsEcsResponse()
	_result, err := i.Client.Cli.DescribeInstanceTypesWithOptions(req.DescribeInstanceTypesRequest, runtime)
	if err != nil {
		logger.L().Debug().Msgf(err.Error())
		return nil, err
	}
	result.DescribeInstanceTypesResponse = _result
	result.Totals = len(_result.Body.InstanceTypes.InstanceType)
	return result, nil
}

func (i *impl) QuerySpecsZoneEcs(ctx context.Context, req *ecs.QuerySpecsZoneEcsRequest) (
	*ecs.QuerySpecsZoneEcsResponse, error) {
	result := ecs.NewQuerySpecsZoneEcsResponse()
	_result, err := i.Client.Cli.DescribeAvailableResource(req.DescribeAvailableResourceRequest)
	if err != nil {
		logger.L().Debug().Msgf(err.Error())
		return nil, err
	}
	for _, v1 := range _result.Body.AvailableZones.AvailableZone {
		for _, v2 := range v1.AvailableResources.AvailableResource {
			for _, v3 := range v2.SupportedResources.SupportedResource {
				result.SpecsZoneEcs = append(result.SpecsZoneEcs, v3.Value)
			}
		}
	}

	result.Totals = len(result.SpecsZoneEcs)
	return result, nil
}
