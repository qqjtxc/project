package impl

import (
	"gitee.com/qqjtxc/project/aliyun/apps"
	"gitee.com/qqjtxc/project/aliyun/apps/client/ecscli"
	"gitee.com/qqjtxc/project/aliyun/apps/ecs"
	"gitee.com/qqjtxc/project/aliyun/logger"
)

// 定义一个结构体实现用户相关方法
type impl struct {
	Client *ecscli.EcsClient
}

// 对实例进行初始化，保护依赖的注入
func (i *impl) Init() error {
	cli, err := ecscli.GetEcsClient()
	if err != nil {
		logger.L().Info().Msgf("获取创建的ECS客户端失败:%s", err)
		return err
	}

	i.Client = cli
	return nil
}

func (i *impl) Name() string {
	return ecs.AppName
}

func init() {
	obj := &impl{}
	// 把对象托管到Ioc的Grpc map中
	// apps.RegistryGrpc(obj)

	// 内部模块
	apps.Registry(obj)
}
