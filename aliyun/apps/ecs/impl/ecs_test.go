package impl_test

import (
	"encoding/json"
	"fmt"
	"testing"
	"time"

	"gitee.com/qqjtxc/project/aliyun/apps/ecs"
)

func TestSelectAllEcs(t *testing.T) {
	req := ecs.NewSelectAllEcsRequest()
	resp, err := svc.SelectAllEcs(ctx, req, runtime)
	list := []string{}
	for _, v := range resp.DescribeInstanceStatusResponse.Body.InstanceStatuses.InstanceStatus {
		list = append(list, *v.InstanceId)
	}
	jsonList, _ := json.Marshal(list)
	t.Log(string(jsonList))
	if err != nil {
		t.Fatal(err)
	}
	// t.Log(resp)
}

func TestQueryEcsDescribeInstances(t *testing.T) {
	req := ecs.NewQueryEcsDescribeInstancesRequest()
	req.DescribeInstancesRequest.SetPageNumber(1)
	req.DescribeInstancesRequest.SetPageSize(50)
	//req.DescribeInstancesRequest.SetPrivateIpAddresses("")
	// req.DescribeInstancesRequest.SetInstanceNetworkType("VPC")
	resp, err := svc.QueryEcsDescribeInstances(ctx, req, runtime)

	if err != nil {
		t.Fatal(err)
	}
	t.Log(resp)
}

func TestQueryPriceEcs(t *testing.T) {
	req := ecs.NewQueryPriceEcsRequest()
	// 规格参数
	//req.DescribePriceRequest.SetInstanceType("ecs.c8i.4xlarge")
	// 磁盘类型cloud：普通云盘 cloud_efficiency：高效云盘 cloud_ssd：SSD云盘 ephemeral_ssd：本地SSD盘 cloud_essd：ESSD云盘。
	// req.DescribePriceRequest.SystemDisk.SetCategory("cloud_essd")
	// 购买时长	[Month|Year|Hour|Week]
	req.DescribePriceRequest.SetPriceUnit("Week")
	req.DescribePriceRequest.SetPeriod(1)
	// 设置最小CPU核数
	req.Cpus = 4
	// 设置最小内存数（GB
	req.MemSize = 8

	// 可用区设置
	//req.DescribePriceRequest.SetZoneId("cn-beijing-k")
	// 系统盘设置
	// req.DescribePriceRequest.SystemDisk.SetCategory("cloud_essd")
	// // 系统盘大小设置(GB)
	// req.DescribePriceRequest.SystemDisk.SetSize(100)
	// 系统盘格式
	// The performance level of the system disk when the disk is an ESSD.
	// This parameter is valid only when the `SystemDiskCategory` parameter is set to cloud_essd. Default value: PL1. Valid values:
	// PL0 PL1 PL2 PL3
	// req.DescribePriceRequest.SystemDisk.SetPerformanceLevel("PL0")
	// 数据盘设置
	// reqDisk := ecs.NewReqDiskMsg()
	// reqDisk.DataDisk.SetCategory("cloud_essd")
	// reqDisk.DataDisk.SetSize(100)
	// DiskDatas, _ := ecs.ReqDiskMsgs(reqDisk.DataDisk)
	// req.DescribePriceRequest.SetDataDisk(DiskDatas)

	resp, err := svc.QueryPriceEcs(ctx, req, runtime)

	if err != nil {
		t.Fatal(err)
	}
	t.Log(resp)
}

func TestQuerySpecsEcs(t *testing.T) {
	req := ecs.NewQuerySpecsEcsRequest()
	// 设置最小CPU核数
	req.DescribeInstanceTypesRequest.SetMinimumCpuCoreCount(8)
	// 设置最大CPU核数
	req.DescribeInstanceTypesRequest.SetMaximumCpuCoreCount(8)
	// 设置最小内存数（GB）
	req.DescribeInstanceTypesRequest.SetMinimumMemorySize(16)
	// 设置最大内存数（GB）
	req.DescribeInstanceTypesRequest.SetMaximumMemorySize(16)
	// 设置实例挂载的本地盘的单盘容量（GB）
	// req.DescribeInstanceTypesRequest.SetMinimumLocalStorageCapacity(100)
	// 设置实例规格分类（General-purpose： 通用型）
	//req.DescribeInstanceTypesRequest.SetInstanceCategory("General-purpose")
	resp, err := svc.QuerySpecsEcs(ctx, req, runtime)

	if err != nil {
		t.Fatal(err)
	}
	for _, v := range resp.DescribeInstanceTypesResponse.Body.InstanceTypes.InstanceType {
		fmt.Printf("规格：%v,cpu:%vC,内存:%vGB\n", *v.InstanceTypeId, *v.CpuCoreCount, *v.MemorySize)
	}
	t.Log(resp.DescribeInstanceTypesResponse)
}

func TestQuerySpecsZoneEcs(t *testing.T) {
	req := ecs.NewQuerySpecsZoneEcsRequest()

	// 设置cpu大小
	req.DescribeAvailableResourceRequest.SetCores(4)
	// 设置内存大小
	req.DescribeAvailableResourceRequest.SetMemory(8)
	// *   cloud: basic disk.
	// *   cloud_efficiency: ultra disk.
	// *   cloud_ssd: standard SSD.
	// *   ephemeral_ssd: local SSD.
	// *   cloud_essd: ESSD.
	//req.DescribeAvailableResourceRequest.SetDataDiskCategory("cloud_essd")

	resp, err := svc.QuerySpecsZoneEcs(ctx, req)
	if err != nil {
		t.Fatal(err)
	}

	t.Log(resp)
}

func TestXxx(t *testing.T) {
	timeNow := time.Now()
	timeEcs, _ := time.Parse("20060102", "20230614")
	sub := int32(timeEcs.Sub(timeNow).Hours())
	t.Log(sub, (sub+48)/24)

}
