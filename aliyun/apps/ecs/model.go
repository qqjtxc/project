package ecs

import (
	ecs20140526 "github.com/alibabacloud-go/ecs-20140526/v3/client"
	"github.com/alibabacloud-go/tea/tea"
)

// 初始化一些必传参数
const (
	// 查询地区
	regionId            = "cn-beijing"
	resourceType        = "instance"
	destinationResource = "InstanceType"
	// 查询可用区
	zoneId = "cn-beijing-k"
	// 系统盘格式
	systemDiskCategory = "cloud_essd"
	// 系统盘大小设置(GB)
	systemDiskSize = int32(100)
	// 系统盘等级
	systemDiskPerformanceLevel = "PL0"
)

// 定义查询所有ECS的请求
type SelectAllEcsRequest struct {
	DescribeInstanceStatusRequest *ecs20140526.DescribeInstanceStatusRequest
}

// 定义查询ECS详细信息的请求
type QueryEcsDescribeInstancesRequest struct {
	DescribeInstancesRequest *ecs20140526.DescribeInstancesRequest
}

// 查询ECS价格请求
type QueryPriceEcsRequest struct {
	DescribePriceRequest *ecs20140526.DescribePriceRequest
	Cpus                 int32
	MemSize              float32
}

// 查询ECS规格请求
type QuerySpecsEcsRequest struct {
	DescribeInstanceTypesRequest *ecs20140526.DescribeInstanceTypesRequest
}

// 查询ECS规格请求
type QuerySpecsZoneEcsRequest struct {
	DescribeAvailableResourceRequest *ecs20140526.DescribeAvailableResourceRequest
}

func NewQuerySpecsZoneEcsRequest() *QuerySpecsZoneEcsRequest {
	return &QuerySpecsZoneEcsRequest{
		DescribeAvailableResourceRequest: &ecs20140526.DescribeAvailableResourceRequest{
			RegionId:            tea.String(regionId),
			DestinationResource: tea.String(destinationResource),
			ZoneId:              tea.String(zoneId),
		},
	}
}
func NewQuerySpecsEcsRequest() *QuerySpecsEcsRequest {
	return &QuerySpecsEcsRequest{
		DescribeInstanceTypesRequest: &ecs20140526.DescribeInstanceTypesRequest{},
	}
}

func NewQueryPriceEcsRequest() *QueryPriceEcsRequest {
	s := &QueryPriceEcsRequest{
		DescribePriceRequest: &ecs20140526.DescribePriceRequest{
			RegionId:     tea.String(regionId),
			ResourceType: tea.String(resourceType),
			SystemDisk: &ecs20140526.DescribePriceRequestSystemDisk{
				Category:         tea.String(systemDiskCategory),
				PerformanceLevel: tea.String(systemDiskPerformanceLevel),
			},
		},
	}
	s.DescribePriceRequest.SystemDisk.SetSize(systemDiskSize)
	return s
}

func NewQueryEcsDescribeInstancesRequest() *QueryEcsDescribeInstancesRequest {
	return &QueryEcsDescribeInstancesRequest{
		DescribeInstancesRequest: &ecs20140526.DescribeInstancesRequest{
			RegionId: tea.String(regionId),
		},
	}
}

func NewSelectAllEcsRequest() *SelectAllEcsRequest {
	return &SelectAllEcsRequest{
		DescribeInstanceStatusRequest: &ecs20140526.DescribeInstanceStatusRequest{
			RegionId: tea.String(regionId),
		},
	}
}

// 保存所有ECS的结果的实例
type SelectAllEcsResponse struct {
	DescribeInstanceStatusResponse *ecs20140526.DescribeInstanceStatusResponse
}

// 保存ECS的结果的实例
type QueryEcsDescribeInstancesResponse struct {
	Ecs   []EcsDescribe
	Total int32
}

type EcsDescribe struct {
	// 实例ID
	InstanceId string
	// 实例描述
	InstanceName string
	// 私有IP地址
	PrimaryIpAddress string
	// 公网IP地址
	IpAddress string
	// cpu配置
	Cpu int32
	// 内存配置
	Memory int32
	// 到期状态
	EcsExpiretime int32
}

type QuerySpecsZoneEcsResponse struct {
	Totals       int
	SpecsZoneEcs []*string
}

func NewQuerySpecsZoneEcsResponse() *QuerySpecsZoneEcsResponse {
	return &QuerySpecsZoneEcsResponse{
		SpecsZoneEcs: []*string{},
	}
}

// 保存查询ECS价格结果的实例
type QueryPriceEcsResponse struct {
	Totals   int
	PriceEcs []SingleEcsPrice
}

// ECS单台价格相关信息
type SingleEcsPrice struct {
	SpecsEcs string
	PriceEcs float32
	Cpu      int32
	Mem      float32
	Disks    []*ecs20140526.DescribePriceRequestDataDisk
}

func NewSingleEcsPrice() *SingleEcsPrice {
	return &SingleEcsPrice{
		Disks: []*ecs20140526.DescribePriceRequestDataDisk{},
	}
}

// DISK信息
// type DiskMsg struct {
// 	DiskNum int32
// 	Size    int32
// }

// 保存查询ECS规格结果的实例
type QuerySpecsEcsResponse struct {
	// 保存具体查出来的结果
	DescribeInstanceTypesResponse *ecs20140526.DescribeInstanceTypesResponse
	//保存总条数
	Totals int
}

func NewQuerySpecsEcsResponse() *QuerySpecsEcsResponse {
	return &QuerySpecsEcsResponse{
		DescribeInstanceTypesResponse: &ecs20140526.DescribeInstanceTypesResponse{},
	}
}

func NewQueryPriceEcsResponse() *QueryPriceEcsResponse {
	return &QueryPriceEcsResponse{
		PriceEcs: []SingleEcsPrice{},
	}
}

func NewSelectAllEcsResponse() *SelectAllEcsResponse {
	return &SelectAllEcsResponse{
		DescribeInstanceStatusResponse: &ecs20140526.DescribeInstanceStatusResponse{},
	}
}

// 数据盘
type ReqDiskMsg struct {
	DataDisk *ecs20140526.DescribePriceRequestDataDisk
}

func NewReqDiskMsg() *ReqDiskMsg {
	return &ReqDiskMsg{DataDisk: &ecs20140526.DescribePriceRequestDataDisk{}}

}

// 给磁盘增加一个返回切片方法
func ReqDiskMsgs(reqDisk *ecs20140526.DescribePriceRequestDataDisk) ([]*ecs20140526.DescribePriceRequestDataDisk, error) {
	diskDatas := []*ecs20140526.DescribePriceRequestDataDisk{}
	diskDatas = append(diskDatas, reqDisk)
	return diskDatas, nil
}
