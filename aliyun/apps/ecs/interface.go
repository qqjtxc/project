package ecs

import (
	"context"

	util "github.com/alibabacloud-go/tea-utils/v2/service"
)

// 封装自己的接口
type Service interface {
	// 获取多台ECS实例的状态信息
	SelectAllEcs(context.Context, *SelectAllEcsRequest, *util.RuntimeOptions) (*SelectAllEcsResponse, error)
	// 查询一台或多台ECS实例的详细信息
	QueryEcsDescribeInstances(context.Context, *QueryEcsDescribeInstancesRequest, *util.RuntimeOptions) (*QueryEcsDescribeInstancesResponse, error)
	// 查询云服务器ECS资源的价格
	QueryPriceEcs(context.Context, *QueryPriceEcsRequest, *util.RuntimeOptions) (*QueryPriceEcsResponse, error)
	// 查询云服务器ECS的实例规格
	QuerySpecsEcs(context.Context, *QuerySpecsEcsRequest, *util.RuntimeOptions) (*QuerySpecsEcsResponse, error)
	// 查询指定可用区云服务器ECS的实例规格
	QuerySpecsZoneEcs(context.Context, *QuerySpecsZoneEcsRequest) (*QuerySpecsZoneEcsResponse, error)
}
