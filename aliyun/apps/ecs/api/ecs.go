package api

import (
	"net/http"
	"strconv"

	"gitee.com/qqjtxc/project/aliyun/apps/ecs"
	"gitee.com/qqjtxc/project/aliyun/logger"
	util "github.com/alibabacloud-go/tea-utils/v2/service"
	"github.com/gin-gonic/gin"
)

var (
	runtime = &util.RuntimeOptions{}
)

// 把HTTP的用户请求 转换成内部的业务逻辑调用
func (h *handler) QuerySpecsEcs(c *gin.Context) {
	// runtime := &util.RuntimeOptions{}
	req, err := ecs.NewQuerySpecsEcsRequestFromGin(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"status": 500, "msg": err.Error()})
	}
	resp, err := h.svc.QuerySpecsEcs(c.Request.Context(), req, runtime)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"status": 500, "msg": err.Error()})
	}
	c.JSON(http.StatusOK, resp)
}

func (h *handler) QueryPriceEcs(c *gin.Context) {
	// 解析前端请求参数
	ginReq, err := ecs.NewQueryPriceEcsRequestFromGin(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"status": 500, "msg": err.Error()})
	}

	req := ecs.NewQueryPriceEcsRequest()
	// 对结果进行换算
	ecsMemSize, _ := strconv.ParseFloat(ginReq.EcsMemSize, 32)
	ecsCpus, _ := strconv.ParseInt(ginReq.EcsCpus, 10, 32)
	ecsPeriod, _ := strconv.ParseInt(ginReq.EcsPeriod, 10, 32)

	// 封装符合内部请求的参数结构
	req.Cpus = int32(ecsCpus)
	req.MemSize = float32(ecsMemSize)
	req.DescribePriceRequest.SetPeriod(int32(ecsPeriod))
	req.DescribePriceRequest.SetPriceUnit(ginReq.EcsPriceUnit)

	resp, err := h.svc.QueryPriceEcs(c.Request.Context(), req, runtime)

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"status": 500, "msg": err.Error()})
	}
	c.JSON(http.StatusOK, resp)
}

func (h *handler) QueryEcsDescribeInstances(c *gin.Context) {
	// 解析前端请求参数
	ginReq, err := ecs.NewQueryEcsDescribeInstancesFromGin(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"status": 500, "msg": err.Error()})
	}
	// 封装内部请求参数
	req := ecs.NewQueryEcsDescribeInstancesRequest()
	pn, _ := strconv.Atoi(ginReq.PageNumber)
	ps, _ := strconv.Atoi(ginReq.PageSize)
	req.DescribeInstancesRequest.SetPageNumber(int32(pn))
	req.DescribeInstancesRequest.SetPageSize(int32(ps))
	if ginReq.IpAddresses != "" {
		req.DescribeInstancesRequest.SetPrivateIpAddresses(ginReq.IpAddresses)
	}

	logger.L().Info().Msgf("%s", req)
	resp, err := h.svc.QueryEcsDescribeInstances(c.Request.Context(), req, runtime)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"status": 500, "msg": err.Error()})
	}
	c.JSON(http.StatusOK, resp)
}
