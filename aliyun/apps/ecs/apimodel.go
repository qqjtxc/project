package ecs

import (
	"strconv"

	"gitee.com/qqjtxc/project/aliyun/logger"
	"github.com/gin-gonic/gin"
)

// 定义所有可能用到的常量
const (
	initMinCPUs    = "2"
	initMaxCpus    = "64"
	initMinMem     = "2"
	initMaxMem     = "128"
	initPageSize   = "10"
	initPageNumber = "1"
)

type QueryEcsRequestFromGin struct {
	EcsMaxCpus string `json:"ecs_max_cpus"`
	EcsMinCpus string `json:"ecs_min_cpus"`
	EcsMaxMem  string `json:"ecs_max_mem"`
	EcsMinMem  string `json:"ecs_min_mem"`
	PageSize   string `json:"page_size"`
	PageNumber string `json:"page_number"`
}

func (s *QueryEcsRequestFromGin) validate() {
	if s.EcsMaxCpus == "" {
		s.EcsMaxCpus = initMaxCpus
	}
	if s.EcsMinCpus == "" {
		s.EcsMinCpus = initMinCPUs
	}
	if s.EcsMaxMem == "" {
		s.EcsMaxMem = initMaxMem
	}
	if s.EcsMinMem == "" {
		s.EcsMinMem = initMinMem
	}
	if s.PageSize == "" {
		s.PageSize = initPageSize
	}
	if s.PageNumber == "" {
		s.PageNumber = initPageNumber
	}
}

func NewQueryEcsRequestFromGin() *QueryEcsRequestFromGin {
	return &QueryEcsRequestFromGin{}
}

// 处理查看规格api接口的请求参数
func NewQuerySpecsEcsRequestFromGin(c *gin.Context) (*QuerySpecsEcsRequest, error) {
	req := NewQuerySpecsEcsRequest()
	ginReq := NewQueryEcsRequestFromGin()
	// 参数解析
	err := c.BindJSON(ginReq)
	if err != nil {
		return nil, err
	}

	// 对结果进行换算
	ecsMinMem, _ := strconv.ParseFloat(ginReq.EcsMinMem, 32)
	ecsMaxMem, _ := strconv.ParseFloat(ginReq.EcsMaxMem, 32)
	ecsMaxCpus, _ := strconv.ParseInt(ginReq.EcsMaxCpus, 10, 32)
	ecsMinCpus, _ := strconv.ParseInt(ginReq.EcsMinCpus, 10, 32)

	// 封装内部请求参数
	req.DescribeInstanceTypesRequest.SetMaximumCpuCoreCount(int32(ecsMaxCpus))
	req.DescribeInstanceTypesRequest.SetMinimumCpuCoreCount(int32(ecsMinCpus))
	req.DescribeInstanceTypesRequest.SetMaximumMemorySize(float32(ecsMaxMem))
	req.DescribeInstanceTypesRequest.SetMinimumMemorySize(float32(ecsMinMem))

	return req, nil

}

// 保存前端请求参数
type QueryEcsPriceRequestFromGin struct {
	// 购买单位周期	[Month|Year|Hour|Week]
	EcsPriceUnit string `json:"ecs_price_unit"`
	// 购买时长
	EcsPeriod string `json:"ecs_period"`
	// cpu核数
	EcsCpus string `json:"ecs_cpus"`
	// 内存大小
	EcsMemSize string `json:"ecs_mem_size"`
	// 分页参数
	PageSize   string `json:"page_size"`
	PageNumber string `json:"page_number"`
}

func NewQueryEcsPriceRequestFromGin() *QueryEcsPriceRequestFromGin {
	return &QueryEcsPriceRequestFromGin{}
}

// 对请求ECS价格时传参进行 BindJSON
func NewQueryPriceEcsRequestFromGin(c *gin.Context) (*QueryEcsPriceRequestFromGin, error) {
	ginReq := NewQueryEcsPriceRequestFromGin()
	// 参数解析
	err := c.BindJSON(ginReq)
	if err != nil {
		logger.L().Info().Msg(err.Error())
		return nil, err
	}
	return ginReq, nil
}

type QueryEcsDescribeInstancesFromGin struct {
	// 支持输入内网IP来查询
	IpAddresses string `json:"ip_addresses"`
	// 分页参数
	PageSize   string `json:"page_size"`
	PageNumber string `json:"page_number"`
}

func NewQueryEcsDescribeInstancesFromGin(c *gin.Context) (*QueryEcsDescribeInstancesFromGin, error) {
	ginReq := &QueryEcsDescribeInstancesFromGin{}
	// 参数解析
	err := c.BindJSON(ginReq)
	if err != nil {
		logger.L().Info().Msg(err.Error())
		return nil, err
	}
	return ginReq, nil
}
