package apps

import (
	"gitee.com/qqjtxc/project/aliyun/logger"
	"github.com/gin-gonic/gin"
)

// 托管所有的http的实例类
var (
	httpAppStore = map[string]httpIocObject{}
)

type httpIocObject interface {
	iocObject
	RegistryHandler(r gin.IRouter)
}

// 1、Registry:	实例的注册
func RegistryHttp(obj httpIocObject) {
	//如果该对象已经存在，还允许注册吗？不允许了！

	//判断对象是否已注册
	if _, ok := httpAppStore[obj.Name()]; ok {
		logger.L().Debug().Msgf("object %s already registried", obj.Name())
		return
	}

	//开始注册
	httpAppStore[obj.Name()] = obj
	logger.L().Debug().Msgf("object %s registried", obj.Name())
}

// 2、获取已经注册的服务
func GetHttp(objName string) interface{} {
	//判断服务是否在ioc 里
	if v, ok := httpAppStore[objName]; ok {
		return v
	}

	logger.L().Debug().Msgf("object %s not found", objName)
	return nil
}
