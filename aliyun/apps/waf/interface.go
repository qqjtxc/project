package waf

import (
	"context"

	util "github.com/alibabacloud-go/tea-utils/v2/service"
)

// 封装自己的接口
type Service interface {
	// 替换域名对应证书
	ReplaceDomainCertificate(context.Context, *ReplaceDomainCertificateRequest,
		*util.RuntimeOptions) (*ReplaceDomainCertificateResponse, error)
	// 获取指定域名可选证书
	QueryDomainCertificate(context.Context, *QueryDomainCertificateRequest,
		*util.RuntimeOptions) (*QueryDomainCertificateResponse, error)
	// 获取waf的实例ID
	QueryWafInstanceId(context.Context, *QueryWafInstanceIdRequest,
		*util.RuntimeOptions) (*QueryWafInstanceIdResponse, error)
	// 获取waf中已添加的域名名称列表
	QueryWafAllDomainList(context.Context, *QueryWafAllDomainListRequest,
		*util.RuntimeOptions) (*QueryWafAllDomainListReponse, error)
	// 批量替换域名证书文件
	ReplaceDoaminSsl(context.Context, *ReplaceDoaminSslRequest,
		*util.RuntimeOptions) (*ReplaceDoaminSslResponse, error)
}
