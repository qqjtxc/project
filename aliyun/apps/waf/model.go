package waf

import (
	waf_openapi20190910 "github.com/alibabacloud-go/waf-openapi-20190910/v2/client"
)

// 定义一些常量
const (
	InstanceId = "waf-cn-2r42vq36k0g"
)

type ReplaceDomainCertificateRequest struct {
}

type ReplaceDomainCertificateResponse struct {
}

type QueryDomainCertificateRequest struct {
	DescribeCertificatesRequest *waf_openapi20190910.DescribeCertificatesRequest
}

type QueryWafInstanceIdRequest struct {
	DescribeInstanceSpecInfoRequest *waf_openapi20190910.DescribeInstanceSpecInfoRequest
}

type QueryWafAllDomainListRequest struct {
	DescribeDomainNamesRequest *waf_openapi20190910.DescribeDomainNamesRequest
}

type ReplaceDoaminSslRequest struct {
	CreateCertificateByCertificateIdRequest *waf_openapi20190910.CreateCertificateByCertificateIdRequest
}

func NewReplaceDoaminSslRequest() *ReplaceDoaminSslRequest {
	return &ReplaceDoaminSslRequest{
		CreateCertificateByCertificateIdRequest: &waf_openapi20190910.CreateCertificateByCertificateIdRequest{},
	}
}

func NewQueryWafAllDomainListRequest() *QueryWafAllDomainListRequest {
	return &QueryWafAllDomainListRequest{
		DescribeDomainNamesRequest: &waf_openapi20190910.DescribeDomainNamesRequest{},
	}
}

func NewQueryWafInstanceIdRequest() *QueryWafInstanceIdRequest {
	return &QueryWafInstanceIdRequest{
		DescribeInstanceSpecInfoRequest: &waf_openapi20190910.DescribeInstanceSpecInfoRequest{},
	}
}

func NewQueryDomainCertificateRequest() *QueryDomainCertificateRequest {
	return &QueryDomainCertificateRequest{
		DescribeCertificatesRequest: &waf_openapi20190910.DescribeCertificatesRequest{},
	}
}

type QueryWafAllDomainListReponse struct {
	DescribeDomainNamesResponse *waf_openapi20190910.DescribeDomainNamesResponse
}

type QueryDomainCertificateResponse struct {
	DescribeCertificatesResponse *waf_openapi20190910.DescribeCertificatesResponse
}

type QueryWafInstanceIdResponse struct {
	DescribeInstanceSpecInfoResponse *waf_openapi20190910.DescribeInstanceSpecInfoResponse
}

type ReplaceDoaminSslResponse struct {
	CreateCertificateByCertificateIdResponse *waf_openapi20190910.CreateCertificateByCertificateIdResponse
}
