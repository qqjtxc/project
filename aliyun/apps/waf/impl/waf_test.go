package impl_test

import (
	"fmt"
	"testing"

	"gitee.com/qqjtxc/project/aliyun/apps/waf"
)

func TestQueryWafInstanceId(t *testing.T) {
	req := waf.NewQueryWafInstanceIdRequest()
	resp, err := svc.QueryWafInstanceId(ctx, req, runtime)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(resp)
}

func TestQueryDomainCertificate(t *testing.T) {
	req := waf.NewQueryDomainCertificateRequest()

	resp, err := svc.QueryDomainCertificate(ctx, req, runtime)
	if err != nil {
		t.Fatal(err)
	}

	t.Log(resp)
}

func TestQueryWafAllDomainList(t *testing.T) {
	req := waf.NewQueryWafAllDomainListRequest()
	// reqDomain := waf.NewReplaceDoaminSslRequest()
	resp, err := svc.QueryWafAllDomainList(ctx, req, runtime)
	// for _, value := range resp.DescribeDomainNamesResponse.Body.DomainNames {
	// 	tmpList := strings.Split(*value, ".")
	// 	if strings.HasSuffix(*value, "XXXXXX") && len(tmpList) == 3 {
	// 		t.Log(*value)
	// 		reqDomain.CreateCertificateByCertificateIdRequest.Domain = value
	// 		reqDomain.CreateCertificateByCertificateIdRequest.SetCertificateId(10305670)
	// 		resp, err := svc.ReplaceDoaminSsl(ctx, reqDomain, runtime)
	// 		if err != nil {
	// 			t.Fatal(err)
	// 		}
	// 		t.Log(resp.CreateCertificateByCertificateIdResponse.Body.GoString())
	// 	} else {
	// 		t.Log("--------", *value)
	// 	}
	// }
	if err != nil {
		fmt.Println(err)
	}
	t.Log(resp)
}

func TestReplaceDoaminSsl(t *testing.T) {
	req := waf.NewReplaceDoaminSslRequest()
	req.CreateCertificateByCertificateIdRequest.SetDomain("XXXXXXXX")
	req.CreateCertificateByCertificateIdRequest.SetCertificateId(10305670)
	resp, err := svc.ReplaceDoaminSsl(ctx, req, runtime)

	if err != nil {
		t.Fatal(err)
	}
	t.Log(resp)
}
