package impl

import (
	"context"

	"gitee.com/qqjtxc/project/aliyun/apps/waf"
	"gitee.com/qqjtxc/project/aliyun/logger"
	util "github.com/alibabacloud-go/tea-utils/v2/service"
)

func (i *impl) QueryDomainCertificate(ctx context.Context, req *waf.QueryDomainCertificateRequest,
	runtime *util.RuntimeOptions) (*waf.QueryDomainCertificateResponse, error) {
	// 先获取waf的实例ID
	idReq := waf.NewQueryWafInstanceIdRequest()
	idResp, err := i.QueryWafInstanceId(ctx, idReq, runtime)
	if err != nil {
		logger.L().Info().Msgf("获取waf实例id失败：%s", err)
		return nil, err
	}
	// 必须传入waf的实例ID才可以获取waf的其他相关信息
	// 这一步是把上面获取的waf实例id作为参数使用
	req.DescribeCertificatesRequest.InstanceId = idResp.DescribeInstanceSpecInfoResponse.Body.InstanceId

	// 请求具体域名可选择的证书
	_resout, _err := i.Client.Cli.DescribeCertificatesWithOptions(req.DescribeCertificatesRequest, runtime)
	if _err != nil {
		logger.L().Info().Msgf("获取域名可选证书失败：%s", _err)
	}

	resp := &waf.QueryDomainCertificateResponse{}
	resp.DescribeCertificatesResponse = _resout
	return resp, nil
}

func (i *impl) QueryWafInstanceId(ctx context.Context, req *waf.QueryWafInstanceIdRequest,
	runtime *util.RuntimeOptions) (*waf.QueryWafInstanceIdResponse, error) {
	_resout, _err := i.Client.Cli.DescribeInstanceSpecInfoWithOptions(req.DescribeInstanceSpecInfoRequest, runtime)
	if _err != nil {
		logger.L().Info().Msgf("请求waf实例id失败:%s", _err)
		return nil, _err
	}

	resp := &waf.QueryWafInstanceIdResponse{}
	resp.DescribeInstanceSpecInfoResponse = _resout

	return resp, nil
}

func (i *impl) ReplaceDomainCertificate(ctx context.Context, req *waf.ReplaceDomainCertificateRequest,
	runtime *util.RuntimeOptions) (*waf.ReplaceDomainCertificateResponse, error) {

	return nil, nil
}

func (i *impl) QueryWafAllDomainList(ctx context.Context, req *waf.QueryWafAllDomainListRequest,
	runtime *util.RuntimeOptions) (*waf.QueryWafAllDomainListReponse, error) {
	// 先获取waf的实例ID
	idReq := waf.NewQueryWafInstanceIdRequest()
	idResp, err := i.QueryWafInstanceId(ctx, idReq, runtime)
	if err != nil {
		logger.L().Info().Msgf("获取waf实例id失败：%s", err)
		return nil, err
	}

	req.DescribeDomainNamesRequest.InstanceId = idResp.DescribeInstanceSpecInfoResponse.Body.InstanceId
	_resout, _err := i.Client.Cli.DescribeDomainNamesWithOptions(req.DescribeDomainNamesRequest, runtime)
	if _err != nil {
		logger.L().Info().Msgf("获取waf全部域名失败：%s", _err)
	}
	resp := &waf.QueryWafAllDomainListReponse{}
	resp.DescribeDomainNamesResponse = _resout
	return resp, nil
}

func (i *impl) ReplaceDoaminSsl(ctx context.Context, req *waf.ReplaceDoaminSslRequest,
	runtime *util.RuntimeOptions) (*waf.ReplaceDoaminSslResponse, error) {

	// 先获取waf的实例ID
	idReq := waf.NewQueryWafInstanceIdRequest()
	idResp, err := i.QueryWafInstanceId(ctx, idReq, runtime)
	if err != nil {
		logger.L().Info().Msgf("获取waf实例id失败：%s", err)
		return nil, err
	}

	req.CreateCertificateByCertificateIdRequest.InstanceId = idResp.DescribeInstanceSpecInfoResponse.Body.InstanceId
	_resout, _err := i.Client.Cli.CreateCertificateByCertificateIdWithOptions(req.CreateCertificateByCertificateIdRequest, runtime)
	if _err != nil {
		logger.L().Info().Msgf("替换域名证书失败：%s", _err)
	}
	resp := &waf.ReplaceDoaminSslResponse{}
	resp.CreateCertificateByCertificateIdResponse = _resout

	return resp, nil
}
