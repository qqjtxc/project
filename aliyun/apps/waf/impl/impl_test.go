package impl_test

import (
	"context"

	"gitee.com/qqjtxc/project/aliyun/apps"
	"gitee.com/qqjtxc/project/aliyun/apps/waf"
	"gitee.com/qqjtxc/project/aliyun/test/tools"
	util "github.com/alibabacloud-go/tea-utils/v2/service"
)

var (
	ctx     = context.Background()
	svc     waf.Service
	runtime = &util.RuntimeOptions{}
)

func init() {
	// 加载测试用例的配置对象
	tools.DevelopmentSet()
	// 从ioc中获取对应的ecs service的具体实现
	svc = apps.GetInternalApp(waf.AppName).(waf.Service)
}
