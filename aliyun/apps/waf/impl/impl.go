package impl

import (
	"gitee.com/qqjtxc/project/aliyun/apps"
	"gitee.com/qqjtxc/project/aliyun/apps/client/wafcli"
	"gitee.com/qqjtxc/project/aliyun/apps/waf"
	"gitee.com/qqjtxc/project/aliyun/logger"
)

// 定义一个结构体实现用户相关方法
type impl struct {
	Client *wafcli.WafClient
}

// 对实例进行初始化，保护依赖的注入
func (i *impl) Init() error {
	cli, err := wafcli.GetWaFClient()
	if err != nil {
		logger.L().Info().Msgf("获取创建的WAF客户端失败:%s", err)
		return err
	}

	i.Client = cli
	return nil
}

func (i *impl) Name() string {
	return waf.AppName
}

func init() {
	obj := &impl{}

	// 内部模块
	apps.Registry(obj)
}
