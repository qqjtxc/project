package api

import (
	"net/http"

	"gitee.com/qqjtxc/project/aliyun/apps/waf"
	util "github.com/alibabacloud-go/tea-utils/v2/service"
	"github.com/gin-gonic/gin"
)

var (
	runtime = &util.RuntimeOptions{}
)

func (h *handler) QueryDomainCertificate(c *gin.Context) {
	req := waf.NewQueryDomainCertificateRequest()

	// 获取前端请求query参数
	domain := c.Query("domain")
	req.DescribeCertificatesRequest.SetDomain(domain)
	// 返回处理结果
	resp, err := h.svc.QueryDomainCertificate(c, req, runtime)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"code:": 500, "msg:": err.Error()})
		return
	}
	c.JSON(http.StatusOK, resp.DescribeCertificatesResponse.Body)
}

func (h *handler) QueryWafInstanceId(c *gin.Context) {
	req := waf.NewQueryWafInstanceIdRequest()
	resp, err := h.svc.QueryWafInstanceId(c, req, runtime)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"code:": 500, "msg:": err.Error()})
		return
	}
	c.JSON(http.StatusOK, resp.DescribeInstanceSpecInfoResponse.Body)
}
