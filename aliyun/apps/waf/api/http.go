package api

import (
	"gitee.com/qqjtxc/project/aliyun/apps"
	"gitee.com/qqjtxc/project/aliyun/apps/waf"
	"github.com/gin-gonic/gin"
)

// 定义一个handler对象 实现 HTTP 接口
// 需要依赖业务逻辑，业务实现类
type handler struct {
	svc waf.Service
}

func (h *handler) Name() string {
	return waf.AppName
}

func (h *handler) Init() error {
	// ioc 依赖，需要注入 ecs service 的实例类
	h.svc = apps.GetInternalApp(waf.AppName).(waf.Service)
	return nil
}

// 这个模块的API的前缀：/ecs/api/v1
// ecs handler 需要暴露的接口 注册给 gin root router

func (h *handler) RegistryHandler(r gin.IRouter) {
	// 不需要认证的接口：可以直接访问
	r.GET("/", h.QueryWafInstanceId)
	r.GET("/httpssl", h.QueryDomainCertificate)
}

// http实例注册
func init() {
	apps.RegistryHttp(&handler{})
}
