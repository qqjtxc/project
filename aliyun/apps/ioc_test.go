package apps_test

import (
	"context"
	"testing"

	aliyun "gitee.com/qqjtxc/project/aliyun/apps"
	"gitee.com/qqjtxc/project/aliyun/apps/ecs"
	util "github.com/alibabacloud-go/tea-utils/v2/service"
)

var (
	ctx     = context.Background()
	runtime = &util.RuntimeOptions{}
)

const (
	AppName = "comments"
)

type AppObj struct {
}

func (a *AppObj) Name() string {
	return AppName
}

func (a *AppObj) Init() error {
	return nil
}

// 查询云服务器ECS的实例规格
func (a *AppObj) QuerySpecsEcs(context.Context, *ecs.QuerySpecsEcsRequest, *util.RuntimeOptions) (*ecs.QuerySpecsEcsResponse, error) {
	return ecs.NewQuerySpecsEcsResponse(), nil
}

func TestXxx(t *testing.T) {
	//对象的注册
	aliyun.Registry(&AppObj{})

	//获取对象
	obj := aliyun.GetInternalApp(AppName)

	//断言类型
	blogSvc := obj.(ecs.Service)

	b, err := blogSvc.QuerySpecsEcs(ctx, nil, runtime)
	if err != nil {
		t.Fatal(err)
	}

	t.Log(b)
}
