package apps

import (
	"gitee.com/qqjtxc/project/aliyun/logger"
	"github.com/gin-gonic/gin"
	"google.golang.org/grpc"
)

// 初始化 ioc 中的模块
// 这个初始化应该在什么时候进行？ 放置main
func InitApp() error {
	for _, v := range internalAppStore {
		if err := v.Init(); err != nil {
			return err
		}
		//补充日志
		logger.L().Debug().Msgf("object %s initial", v.Name())
	}
	return nil
}

// http 初始化
func InitHttpApps(r gin.IRouter) error {
	for _, v := range httpAppStore {
		//对实例进行初始化
		if err := v.Init(); err != nil {
			return err
		}
		logger.L().Debug().Msgf("object %s initial", v.Name())
		v.RegistryHandler(r.Group(v.Name()))
	}
	return nil
}

// grpc 初始化
func InitGrpcApps(server *grpc.Server) error {
	for _, v := range grpcAppStore {
		//对实例进行初始化
		if err := v.Init(); err != nil {
			return err
		}
		logger.L().Debug().Msgf("object %s initial", v.Name())
		v.RegistryHandler(server)
	}
	return nil
}
