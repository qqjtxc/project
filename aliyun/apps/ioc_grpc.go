package apps

import (
	"gitee.com/qqjtxc/project/aliyun/logger"
	"google.golang.org/grpc"
)

// 托管所有的grpc的实例类
var (
	grpcAppStore = map[string]grpcIocObject{}
)

type grpcIocObject interface {
	iocObject
	RegistryHandler(*grpc.Server)
}

// 1、Registry:	实例的注册
func RegistryGrpc(obj grpcIocObject) {
	//判断对象是否已注册
	if _, ok := grpcAppStore[obj.Name()]; ok {
		logger.L().Debug().Msgf("object %s already registried", obj.Name())
		return
	}

	//开始注册
	grpcAppStore[obj.Name()] = obj
	logger.L().Debug().Msgf("object %s registried", obj.Name())
}

// 2、获取已经注册的服务
func GetGrpc(objName string) interface{} {
	//判断服务是否在ioc 里
	if v, ok := grpcAppStore[objName]; ok {
		return v
	}

	logger.L().Debug().Msgf("object %s not found", objName)
	return nil
}
