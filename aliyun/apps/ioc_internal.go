package apps

import (
	"gitee.com/qqjtxc/project/aliyun/logger"
)

// 托管所有的impl的实例类
var (
	internalAppStore = map[string]iocObject{}
)

type iocObject interface {
	// 需要获取对应注册的名称
	Name() string
	// 体现对象初始化能力
	Init() error
}

// 1、Registry:实例的注册
func Registry(obj iocObject) {
	//如果该对象已经存在，还允许注册吗？不允许了！
	//判断对象是否已经注册
	if _, ok := internalAppStore[obj.Name()]; ok {
		//panic(obj.Name() + "has regisried")
		logger.L().Debug().Msgf("object %s already registried", obj.Name())
		return
	}

	// 存入store
	internalAppStore[obj.Name()] = obj

	// 2.补充日志
	logger.L().Debug().Msgf("object %s registried", obj.Name())
}

// 2、Get：通过名称获取实例类
func GetInternalApp(objName string) interface{} {
	// 如果获取不到对象是不是应该报错？可以返回404
	if v, ok := internalAppStore[objName]; ok {
		return v
	}

	// 2.补充日志
	logger.L().Debug().Msgf("object %s not found", objName)
	return nil
}
