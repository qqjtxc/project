package main

import (
	"fmt"

	"github.com/shirou/gopsutil/v3/cpu"
	"github.com/shirou/gopsutil/v3/disk"
	"github.com/shirou/gopsutil/v3/host"
	"github.com/shirou/gopsutil/v3/mem"
)

func main() {
	v, err := mem.VirtualMemory()
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Printf("Total:%vM,\nFree:%vM,\nUsedPercent:%f%%\n", v.Total/1024/1024, v.Free/1024/1024, v.UsedPercent)

	cpuInfos, _ := cpu.Info()
	for _, item := range cpuInfos {
		fmt.Printf("cores:%v\n", item.Cores)
	}

	hostInfo, _ := host.Info()
	fmt.Printf("hostname:%v,\nos:%v\n", hostInfo.Hostname, hostInfo.OS)

	parts, _ := disk.Partitions(true)
	for _, part := range parts {
		fmt.Println(part.Device)
	}

	disgMsg, _ := disk.Usage("/")
	fmt.Printf("磁盘大小：%dG\n", disgMsg.Total/1024/1024/1024)

	// netIOs, _ := net.IOCounters(true)
	// fmt.Println("netIOs", netIOs)
}
