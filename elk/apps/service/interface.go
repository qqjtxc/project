package service

import "context"

// 封装自己的接口
type Svc interface {
	//查询索引
	QueryIndex(*context.Context, *QueryIndexRequest) error
}

type QueryIndexRequest struct {
	Address string
}

func NewQueryIndexRequest(addr string) *QueryIndexRequest {

	return &QueryIndexRequest{
		Address: addr,
	}
}
