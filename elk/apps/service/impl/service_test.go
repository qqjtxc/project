package impl_test

import (
	"context"
	"testing"

	"gitee.com/qqjtxc/project/elk/apps/service"
	"gitee.com/qqjtxc/project/elk/apps/service/impl"
)

var (
	ins = &impl.Impl{}
	ctx = context.Background()
)

func TestQueryIndex(t *testing.T) {
	req := service.NewQueryIndexRequest("http://xdlog.seentao.com/elk/_cat/indices?v")
	err := ins.QueryIndex(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("ok")
}
