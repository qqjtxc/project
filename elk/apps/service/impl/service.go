package impl

import (
	"context"
	"fmt"
	"net/http"

	"gitee.com/qqjtxc/project/elk/apps/service"
)

func (i *Impl) QueryIndex(ctx context.Context, req *service.QueryIndexRequest) error {
	resp, err := http.Get(req.Address)
	if err != nil {
		return nil
	}
	fmt.Println(resp)
	return nil
}
