package config

var (
	EtcFilePath = "/Users/xuchi/go/project/project/elk/etc/elk.toml"
)

// 保存项目所需配置信息
type Config struct {
	ElkConf *ElkConf `toml:elk`
}

func NewConfig() *Config {
	return &Config{
		ElkConf: NewElkConf(),
	}
}

// toml中针对elk对象设置
type ElkConf struct {
	Endpoint string `toml:"endpoint"`
}

func NewElkConf() *ElkConf {
	return &ElkConf{}
}
