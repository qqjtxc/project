package config

import "github.com/BurntSushi/toml"

var (
	configMsg *Config
)

// 判断全局变量
func ChangeConfig() *Config {
	if configMsg == nil {
		panic("load config fail")
	}
	return configMsg
}

// 从toml文件中读取配置信息
func LoadConfigFromToml(filepath string) error {
	configMsg = NewConfig()

	_, err := toml.DecodeFile(filepath, configMsg)
	if err != nil {
		return err
	}
	return nil
}
