package job_test

import (
	"testing"

	"gitee.com/qqjtxc/project/jenkins/job"
)

func TestDescribeJob(t *testing.T) {
	reqStr := "assima-service"

	resp, err := job.DescribeJob(reqStr)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(resp.StatusCode)
}
