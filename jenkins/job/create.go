package job

import (
	"fmt"
	"net/http"
	"os/exec"

	"gitee.com/qqjtxc/project/jenkins/logger"
	"gitee.com/qqjtxc/project/jenkins/validator"
	"github.com/gin-gonic/gin"
)

const (
	// 创建job脚本文件
	scriptFileName = "./create_job.sh"

	// jenkins地址
	url = "http://xuchi:11ae9dc039d149cdd0d20424001345a71e@10.10.16.163:8080/jenkins"
)

type GinJobMsg struct {
	// 前后端标识
	OpsName string `json:"ops_name" validate:"required"`
	// 视图名称
	ViewsName string `json:"views_name" validate:"required"`
	// 项目名字
	ProjectName string `json:"project_name" validate:"required"`
	// 项目使用端口
	ProjectPort string `json:"project_port" validate:"required"`
	// 项目负责人
	ProjectPeople string `json:"project_people" validate:"required"`
	// 项目git地址
	ProjectAddress string `json:"project_address" validate:"required"`
	// 项目部署分支
	ProjectBranch string `json:"project_branch" validate:"required"`
}

// 数据验证
func (s *GinJobMsg) Validate() error {
	return validator.V().Struct(s)
}

func NewGinJobMsg() *GinJobMsg {
	return &GinJobMsg{}
}

// 检查job是否已存在
func DescribeJob(jobname string) (*http.Response, error) {
	tmpUrl := url + "/job/" + jobname + "/api/json"
	resp, err := http.Get(tmpUrl)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

// 调用shell创建job
func CreateJob(c *gin.Context) {
	logger.L().Debug().Msg("开始请求数据")
	// 获取前端数据
	req := NewGinJobMsg()
	if err := c.BindJSON(req); err != nil {
		logger.L().Debug().Msg(err.Error())
		c.JSON(http.StatusBadRequest, gin.H{"code": http.StatusBadRequest, "msg": err.Error()})
		return
	}
	// 校验数据
	if err := req.Validate(); err != nil {
		logger.L().Debug().Msg(err.Error())
		c.JSON(http.StatusBadRequest, gin.H{"code": http.StatusBadRequest, "msg": err.Error()})
		return
	}

	// 判断job是否已经存在，存在直接返回 已存在
	resp, err := DescribeJob(req.ProjectName)
	if err != nil {
		logger.L().Debug().Msg(err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{"code": http.StatusInternalServerError, "msg": err.Error()})
		return
	}
	if resp.StatusCode == 200 {
		c.JSON(http.StatusBadRequest, gin.H{"code": http.StatusBadRequest, "msg": fmt.Errorf(req.ProjectName + "已存在").Error()})
		return
	}

	// 创建job
	cmd := exec.Command("sh", scriptFileName, req.OpsName, req.ViewsName, req.ProjectName, req.ProjectPort, req.ProjectAddress, req.ProjectBranch)

	output, err := cmd.Output()
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"code": http.StatusInternalServerError, "msg": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"code": http.StatusOK, "msg": string(output)})
}
