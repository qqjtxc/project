package main

import (
	"gitee.com/qqjtxc/project/jenkins/job"
	"github.com/gin-gonic/gin"
)

// 构建服务监听
func main() {
	server := gin.Default()
	v1 := server.Group("/jenkins/api/v1")
	v1.POST("/create/job", job.CreateJob)
	server.Run(":10000")
}
