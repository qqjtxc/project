package validator

import "github.com/go-playground/validator/v10"

// 自定义校验
var (
	validate *validator.Validate
)

func V() *validator.Validate {
	return validate
}

func init() {
	validate = validator.New()
	// 可以做一些其他的设置
}
