package service

// 客户端: client.Call("ChatRpc.Greet", "Tom", &resp)  --> Greet()
// Server端口: Greet(request string, response *string) error
type HelloService interface {
	Greet(request string, response *string) error
}
