package main

import (
	"fmt"
	"net/rpc"

	"gitee.com/qqjtxc/project/micro/rpc/interface/service"
)

// 接口约束
var _ service.HelloService = (*ChatRpc)(nil)

// 定义一个类,客户端的一个SDK
type ChatRpc struct {
	rpc *rpc.Client
}

// address 服务端地址
func NewChatRpcClient(addr string) *ChatRpc {
	// 首先是通过rpc.Dial拨号RPC服务, 建立连接
	// 封装了 tcp 的rpc
	client, err := rpc.Dial("tcp", addr)
	if err != nil {
		panic(err)
	}
	return &ChatRpc{
		rpc: client,
	}
}

// 封装底层的Rpc逻辑, 调用原生请求
func (c *ChatRpc) Greet(request string, response *string) error {
	//  建立连接后，如何调用Server function
	return c.rpc.Call("ChatRpc.Greet", request, response)
}

func main() {
	//初始化一个客户端
	client := NewChatRpcClient("localhost:1234")
	var resp string
	client.Greet("tom", &resp)

	fmt.Println(resp)
}
