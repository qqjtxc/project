package main

import (
	"fmt"
	"log"
	"net"
	"net/rpc"

	"gitee.com/qqjtxc/project/micro/rpc/interface/service"
)

// 实现一个Greet
// ChatRpc 类, 需要被 HelloService 接口约束

// var a int = 10
// a 这个变量不用的, 只用接口来完成约束
// var a service.HelloService = &ChatRpc{}
// var _ service.HelloService = &ChatRpc{}
// &ChatRpc{} 这个值也是没用的, 可以使用 空指针代替, 该对象的指针
// (*ChatRpc)(nil) --> 类型转换(断言)
// int64(64) <==> (int64)(64)  (*int64)(nil)  (*ChatRpc)(nil)
// 初始化一个类型为ChatRpc的空指针
// 接口约束
var _ service.HelloService = (*ChatRpc)(nil)

type ChatRpc struct {
}

// 功能实现
// 注册的RPC的实现,  必须要满足RPC规范：
// 由于我们是一个rpc服务, 因此参数上面还是有约束： Greet(request string, response string) error
//
//		第一个参数是请求
//		第二个参数是响应
//	 最后返回Error
//
// 可以类比Http handler
func (c *ChatRpc) Greet(request string, response *string) error {
	*response = fmt.Sprintf("hello %s", request)
	return nil
}

// 进程内调用: ChatRpc{}.Greet()
// 需要引入RPC框架, 来帮我们处理 底层通信/数据交互
// Go 语言内置的RPC net/rpc

func main() {
	// 1. 需要把业务功能类, 注册给RPC框架
	// 其中rpc.Register函数调用会将对象类型中所有满足RPC规则的对象方法注册为RPC函数，
	// 所有注册的方法会放在“ChatRpc”服务空间之下, ChatRpc.Greet
	err := rpc.RegisterName("ChatRpc", new(ChatRpc))
	if err != nil {
		log.Fatal("rpc RegisterName err:", err)
		return
	}

	// rpc框架需要对外提供 API
	// 然后我们建立一个唯一的TCP链接，
	listener, err := net.Listen("tcp", ":1234")
	if err != nil {
		log.Fatal("Listen 失败", err)
		return
	}

	// 通过rpc.ServeConn函数在该TCP链接上为对方提供RPC服务。
	// 每Accept一个请求，就创建一个goroutie进行处理
	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Fatal("listener err:", err)
		}

		// 需要处理该客户端, 发送给我们的请求
		// rpc 的ServeConn 把请求转接给rpc框架给我们处理(底层通信/数据交互)
		go rpc.ServeConn(conn)
	}
}
