package main

import (
	"fmt"
	"log"
	"net/rpc"
)

func main() {
	// 首先是通过rpc.Dial拨号RPC服务, 建立连接
	// 封装了 tcp 的rpc
	client, err := rpc.Dial("tcp", "localhost:1234")
	if err != nil {
		log.Fatal("rpc Dial err:", err)
		return
	}
	// 建立连接后，如何调用Server function
	var response string
	err = client.Call("ChatRpc.Greet", "tom", &response)
	if err != nil {
		log.Fatal("client Call err:", err)
		return
	}
	fmt.Println(response)
}
