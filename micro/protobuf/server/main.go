package main

import (
	"context"
	"log"
	"net"

	"gitee.com/qqjtxc/project/micro/protobuf/pb"
	"google.golang.org/grpc"
)

// BlogServiceServer is the server API for BlogService service.
// All implementations must embed UnimplementedBlogServiceServer
// for forward compatibility
// type BlogServiceServer interface {
// 	QueryBlog(context.Context, *QueryBlogRequest) (*BlogSet, error)
// 	mustEmbedUnimplementedBlogServiceServer()
// }

var _ pb.BlogServiceServer = (*BlogServiceServerImpl)(nil)

type BlogServiceServerImpl struct {
	pb.UnimplementedBlogServiceServer
}

func (i *BlogServiceServerImpl) QueryBlog(ctx context.Context, in *pb.QueryBlogRequest) (
	*pb.BlogSet, error) {
	return &pb.BlogSet{
		Items: []*pb.Blog{
			{
				Id: 1,
			},
			{
				Id: 2,
			},
		},
	}, nil
}

func main() {
	// 首先是通过grpc.NewServer()构造一个gRPC服务对象
	grpcServer := grpc.NewServer()
	pb.RegisterBlogServiceServer(grpcServer, new(BlogServiceServerImpl))

	listen, err := net.Listen("tcp", ":1234")
	if err != nil {
		log.Fatal(err)
	}

	// 然后通过grpcServer.Serve(lis)在一个监听端口上提供gRPC服务
	grpcServer.Serve(listen)
}
