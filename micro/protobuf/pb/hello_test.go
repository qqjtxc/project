package pb_test

import (
	"testing"

	"gitee.com/qqjtxc/project/micro/protobuf/pb"
	"google.golang.org/protobuf/proto"
)

func TestMarshal(t *testing.T) {
	obj := &pb.String{
		Value: "test",
	}

	data, err := proto.Marshal(obj)
	if err != nil {
		t.Fatal(string(data))
	}
	// json: {"value": "test"}
	// 二进制: <filed number>:value
	t.Log(data)

	new_obj := &pb.String{}
	err = proto.Unmarshal(data, new_obj)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(new_obj)
}

func TestEnum(t *testing.T) {
	obj := &pb.Blog{
		Id: 0,
	}
	t.Log(obj.Id)
}
