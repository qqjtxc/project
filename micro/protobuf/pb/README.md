# mac 安装protobuf

+ 安装
+ + 安装最新版
```sh
brew install protobuf
```
+ + 安装指定版本
```sh
brew install protobuf@3.7
```

+ 测试命令是否安装成功
```sh
xuchi@xuchideMacBook-Pro pb % protoc --version
libprotoc 3.19.4
```

# 生成go文件
+ 报错处理
```sh
% protoc -I=. --go_out=. --go_opt=module="gitee.com/qqjtxc/project/micro/protobuf/pb" hello.proto   
protoc-gen-go: program not found or is not executable
Please specify a program using absolute path or make sure the program is available in your PATH system variable
--go_out: protoc-gen-go: Plugin failed with status code 1.
```
上面的问题需要在mac 的 ~/.zshrc 文件添加环境变量

# 安装protoc-gen-go
go install 输出目录始终为 GOPATH 下的 bin 目录
GOPATH 下的 bin 目录放置的是使用 go install 生成的可执行文件，可执行文件的名称来自于编译时的包名
```sh
go install google.golang.org/protobuf/cmd/protoc-gen-go@latest
```

# grpc
+ 我们看看protobuf 定义接口的语法:
```protobuf
  service <service_name> {
    rpc <function_name> (<request>) returns (<response>);
}
```
+ + service: 用于申明这是个服务的接口
+ + service_name: 服务的名称,接口名称
+ + function_name: 函数的名称
+ + request: 函数参数， 必须的
+ + response: 函数返回， 必须的, 不能没有
  
# grpc 生成代码
```sh
protoc -I=. --go_out=. --go_opt=module="gitee.com/qqjtxc/project/micro/protobuf/pb" hello.proto \
--go-grpc_out=. --go-grpc_opt=module="gitee.com/qqjtxc/project/micro/protobuf/pb" \
hello.proto hello_rpc.proto
```

```sh
protoc -I=. --go_out=. --go_opt=module="gitee.com/qqjtxc/project/micro/protobuf/pb" hello.proto \
--go-grpc_out=. --go-grpc_opt=module="gitee.com/qqjtxc/project/micro/protobuf/pb" *.proto
```

