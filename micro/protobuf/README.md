# 编译proto文件
```sh
protoc -I=. --go_out=./pb --go_opt=module="gitee.com/infraboard/go-course/day21/pb" pb/hello.proto
```
+ -I：-IPATH, --proto_path=PATH, 指定proto文件搜索的路径, 如果有多个路径 可以多次使用-I 来指定, 如果不指定默认为当前目录
+ --go_out: --go指插件的名称, 我们安装的插件为: protoc-gen-go, 而protoc-gen是插件命名规范, go是插件名称, 因此这里是--go, 而--go_out 表示的是 go插件的 out参数, 这里指编译产物的存放目录
+ --go_opt: protoc-gen-go插件opt参数, 这里的module指定了go module, 生成的go pkg 会去除掉module路径，生成对应pkg
+ pb/hello.proto: 我们proto文件路径


# mac 安装protobuf

+ 安装
+ + 安装最新版
```sh
brew install protobuf
```
+ + 安装指定版本
```sh
brew install protobuf@3.7
```

+ 测试命令是否安装成功
```sh
xuchi@xuchideMacBook-Pro pb % protoc --version
libprotoc 3.19.4
```

# 生成go文件
+ 报错处理
```sh
xuchi@xuchideMacBook-Pro pb % protoc -I=. --go_out=. --go_opt=module="gitee.com/qqjtxc/project/micro/protobuf/pb" hello.proto   
protoc-gen-go: program not found or is not executable
Please specify a program using absolute path or make sure the program is available in your PATH system variable
--go_out: protoc-gen-go: Plugin failed with status code 1.
```
上面的问题需要在mac 的 ~/.zshrc 文件添加环境变量

# 安装protoc-gen-go
go install 输出目录始终为 GOPATH 下的 bin 目录
GOPATH 下的 bin 目录放置的是使用 go install 生成的可执行文件，可执行文件的名称来自于编译时的包名
```sh
go install google.golang.org/protobuf/cmd/protoc-gen-go@latest
```

# 安装protoc-gen-go-grpc
```sh
# protoc-gen-go 插件之前已经安装
# go install google.golang.org/protobuf/cmd/protoc-gen-go@latest

# 安装protoc-gen-go-grpc插件
go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@latest
```

