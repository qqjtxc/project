package rpc_test

import (
	"context"
	"testing"

	"gitee.com/qqjtxc/project/devcloud/mcenter/apps/token"
	"gitee.com/qqjtxc/project/devcloud/mcenter/client/rpc"
)

var (
	client *rpc.ClientSet
	ctx    = context.Background()
)

func TestValidateToken(t *testing.T) {
	req := token.NewValidateTokenRequest("cj3hmbuv9mcb2d3luha0")
	t.Log(req)
	tk, err := client.Token().ValidateToken(ctx, req)
	// logger.L().Debug().Msgf("检查token:", err)
	t.Log("TestValidateToken:", err)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tk)
}

func init() {
	conf := rpc.NewDefaultConfig()
	// conf.ClientID = "ci8qk9uv9mc5r1gdavhg"
	// conf.ClientSecret = "ci8qk9uv9mc5r1gdavi0"
	conf.ClientID = "ci8qk9uv9mc5r1gdavhg"
	conf.ClientSecret = "ci8qk9uv9mc5r1gdavi0"
	c, err := rpc.NewClient(conf)
	if err != nil {
		panic(err)
	}
	client = c
}
