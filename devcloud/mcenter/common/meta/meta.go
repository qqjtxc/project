package meta

import (
	"time"

	"github.com/rs/xid"
)

// 扩展protobuf 生成的结构体的方法
func NewMeta() *Meta {
	return &Meta{
		Id:       xid.New().String(),
		CreateAt: time.Now().Unix(), //返回一个时间戳
	}
}
