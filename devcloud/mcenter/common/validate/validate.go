package validate

import "github.com/go-playground/validator/v10"

// 自定校验
var (
	validate *validator.Validate
)

func V() *validator.Validate {
	return validate
}

func init() {
	validate = validator.New()
	//做一些其他的设置
}
