package format

import "encoding/json"

// MarshalIndent类似Marshal但会使用缩进将输出格式化。
func Prettify(i any) string {
	resp, _ := json.MarshalIndent(i, "", "   ")
	return string(resp)
}
