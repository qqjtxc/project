package auth

import (
	"fmt"
	"strings"

	"gitee.com/qqjtxc/project/devcloud/mcenter/apps/endpoint"
	"gitee.com/qqjtxc/project/devcloud/mcenter/apps/token"
	"gitee.com/qqjtxc/project/devcloud/mcenter/common/logger"
	"github.com/emicklei/go-restful/v3"
	"github.com/infraboard/mcube/app"
	"github.com/infraboard/mcube/exception"
	"github.com/infraboard/mcube/http/response"
)

func NewHttpAuther() *HttpAuther {
	return &HttpAuther{
		token: app.GetInternalApp(token.AppName).(token.Service),
	}
}

type HttpAuther struct {
	token token.Service
}

func (a *HttpAuther) AuthFunc(
	req *restful.Request,
	resp *restful.Response,
	next *restful.FilterChain) {
	// 权限判定，用户当前访问了哪个接口（）
	accessRouter := req.SelectedRoute()

	ep := &endpoint.Endpoint{
		Spec: &endpoint.CreateEndpoingRequest{
			// 自己想办法补充，生成service client client_id=serverce_id
			// 自己调用 RPC 查询自己的service id和其他配置信息
			ServiceId: "",
			Method:    accessRouter.Method(),
			Path:      accessRouter.Path(),
			Operation: accessRouter.Operation(),
		},
	}

	// 补充Meta信息
	isAuth := accessRouter.Metadata()["auth"]
	if isAuth != nil {
		// 接口可以断言成任何类型
		ep.Spec.Auth = isAuth.(bool)
	}
	fmt.Println(ep)

	// 认证开启后才校验认证
	if ep.Spec.Auth {
		// 请求拦截并处理
		// 检查Token令牌 如果不合法 Authorization:breaer xxxxx

		// 从 Header 中获取token
		authHeader := req.HeaderParameter(token.TOKEN_HEADER_KEY)
		tk1 := strings.Split(authHeader, " ")
		if len(tk1) != 2 {
			response.Failed(resp, exception.NewUnauthorized("令牌不合法，格式： Authorization: breaer xxxx"))
			return
		}

		tk := tk1[1]
		logger.L().Debug().Msgf("get token: %s", tk)

		// 检查Token的合法性，需要通过RPC进行检查
		tkObj, err := a.token.ValidateToken(req.Request.Context(), token.NewValidateTokenRequest(tk))
		if err != nil {
			response.Failed(resp, exception.NewUnauthorized("令牌校验不合法，%s", err))
			return
		}

		// 放入上下文中
		req.SetAttribute(token.ATTRIBUTE_TOKEN_KEY, tkObj)
	}

	// 交给下个处理
	next.ProcessFilter(req, resp)
}
