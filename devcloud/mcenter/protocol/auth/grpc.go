package auth

// GRPC 服务端中间件

import (
	"context"
	"fmt"

	"gitee.com/qqjtxc/project/devcloud/mcenter/apps/service"
	"github.com/infraboard/mcube/app"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

func NewGrpcAuther() *grpcAuther {
	return &grpcAuther{
		service: app.GetInternalApp(service.AppName).(service.ServiceManager),
	}
}

type grpcAuther struct {
	service service.ServiceManager
}

// 处理数组越界
func (a *grpcAuther) GetClientCredentialsFromMeta(md metadata.MD) (clientId, clientSecret string) {
	cids := md.Get(service.ClientHeaderKey)
	sids := md.Get(service.ClientSecretKey)

	if len(cids) > 0 {
		clientId = cids[0]
	}
	if len(sids) > 0 {
		clientSecret = sids[0]
	}
	return
}

// GRPC 服务端Auth 中间件
// UnaryServerInterceptor provides a hook to intercept the execution of a unary RPC on the server. info
// contains all the information of this RPC the interceptor can operate on. And handler is the wrapper
// of the service method implementation. It is the responsibility of the interceptor to invoke handler
// to complete the RPC.
func (a *grpcAuther) AuthFunc(
	ctx context.Context,
	req interface{},
	info *grpc.UnaryServerInfo,
	handler grpc.UnaryHandler) (resp interface{}, err error) {
	// 获取客户端凭证，也放置Header(meta)，meta存放在ctx中
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return nil, status.Error(codes.Aborted, "获取凭证失败，需要认证")
	}

	clientId, clientSecret := a.GetClientCredentialsFromMeta(md)
	if clientId == "" || clientSecret == "" {
		return nil, status.Error(codes.Aborted, "未传凭证，需要认证")
	}

	svc, err := a.service.DescribeService(ctx, &service.DescribeServiceRequest{
		DescribeBy:    service.DESCRIBE_BY_SERVICE_CRENDENTAIL_ID,
		DescribeVaule: clientId,
	})

	if err != nil {
		return nil, status.Errorf(codes.Aborted, "认证异常：%s", err)
	}

	if clientSecret != svc.Credentail.ClientSecret {
		return nil, status.Errorf(codes.Aborted, "认证失败")
	}

	fmt.Println("============================")
	// 处理其他请求
	resp, err = handler(ctx, req)
	fmt.Println("-------------------------")
	return resp, nil
}
