# 用户中心(IDAS: ID As Service)
+ 认证
  + 登录: 换取身份令牌(Token)
    + UserPassword: 基于用户名密码来获取身份令牌
    + LDAP: 中心化的用户认证系统
    + 企业办公(Feishu/Dingding/企业微信)
  
## 技术
开发2套接口：
  + RESTful接口: 用于Web
     + Go Restful: 专门用于开发Restful API, K8s的API Server 就是用这个框架开发
  + 内部接口: GRPC, 用户认证中间件
    + GRPC 相关开发
  + 数据存储
    + MongoDB(自己更换成MySQL)
  + 参考连接[github.com/infraboard/mcenter/tree/master/apps/token/providrt]
  
## protobuf 版本管理
protobuf 文件是放到 公共代码仓库: mcube, 下载mcube到本地, 对应的protobuf定义 copy过来
找到mcube文件(go module)
```sh
go get github.com/infraboard/mcube@v1.9.7
cp $GOPATH/pkg/mod/github.com/infraboard/mcube@v1.9.7/pb/* mcenter/common/pb/github.com/infraboard/mcube/pb
```


有用链接
(swagger文档连接)[https://petstore.swagger.io/#/%E7%99%BB%E5%BD%95/IssueToken]
