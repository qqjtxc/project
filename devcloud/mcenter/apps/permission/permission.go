package permission

func NewCheckPermissionRequest() *CheckPermissionRequest {
	return &CheckPermissionRequest{}
}

func NewCheckPermissionResponse() *CheckPermissionResponse {
	return &CheckPermissionResponse{}
}
