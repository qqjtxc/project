package impl

import (
	"context"

	"gitee.com/qqjtxc/project/devcloud/mcenter/apps/permission"
	"gitee.com/qqjtxc/project/devcloud/mcenter/apps/policy"
)

func (i *impl) CheckPermission(ctx context.Context, in *permission.CheckPermissionRequest) (*permission.CheckPermissionResponse, error) {
	// 1、查询出用户的策略
	policyQuery := policy.NewQueryPolicyRequest()
	policyQuery.UserId = in.UserId
	policyQuery.Namespace = in.Namespace
	policyQuery.WithRole = true
	ps, err := i.policy.QueryPolicy(ctx, policyQuery)
	if err != nil {
		return nil, err
	}

	checkResp := permission.NewCheckPermissionResponse()
	// 2、根据该用户的角色，判断权限
	roles := ps.GetRoles()
	for i := range roles {
		r := roles[i]
		// 根据传过来的数据 和查到的权限数据进行对比 确定是否有相关权限
		if r.HasFeatrue(in.ServiceId, in.HttpMethod, in.HttpPath) {
			checkResp.HasPermission = true
			checkResp.Role = r
			return checkResp, nil
		}
	}
	return checkResp, nil
}
