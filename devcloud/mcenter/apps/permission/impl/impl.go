package impl

import (
	"gitee.com/qqjtxc/project/devcloud/mcenter/apps/permission"
	"gitee.com/qqjtxc/project/devcloud/mcenter/apps/policy"
	"gitee.com/qqjtxc/project/devcloud/mcenter/conf"
	"github.com/infraboard/mcube/app"
	"go.mongodb.org/mongo-driver/mongo"
	"google.golang.org/grpc"
)

var (
	svc = &impl{}
)

// user service 的实例类
// 之前是通过本地 ioc 进行实例类托管的，现在 ioc 抽象到一个公共代码库管理
// mcube app
type impl struct {
	permission.UnimplementedRPCServer

	// 依赖数据库，在配置对象里面
	col *mongo.Collection

	policy policy.Service
}

func (i *impl) Registry(server *grpc.Server) {
	permission.RegisterRPCServer(server, i)
}

// 实例类初始化
func (i *impl) Config() error {
	db, err := conf.C().Mongo.GetDB()
	if err != nil {
		return err
	}

	i.col = db.Collection(permission.AppName)

	// 接口依赖的初始化
	i.policy = app.GetInternalApp(policy.AppName).(policy.Service)
	return nil
}

func (i *impl) Name() string {
	return permission.AppName
}

func init() {
	// 应用公共库，mcube app
	// 注册grpc托管类
	app.RegistryGrpcApp(svc)
	// 注册内部服务托管
	app.RegistryInternalApp(svc)
}
