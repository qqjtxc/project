package impl_test

import (
	"testing"

	"gitee.com/qqjtxc/project/devcloud/mcenter/apps/permission"
)

func TestCheckPermission(t *testing.T) {
	req := permission.NewCheckPermissionRequest()
	// 哪个用户，在哪个空间下，对什么服务，有什么样的方法，请求什么路径接口的权限
	req.UserId = "cj1220mv9mc9scksji60"
	req.Namespace = "default"
	req.ServiceId = "service01"
	req.HttpMethod = "POST"
	req.HttpPath = "/mcenter"
	ins, err := impl.CheckPermission(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)
}
