package impl

import (
	"context"

	"gitee.com/qqjtxc/project/devcloud/mcenter/apps/policy"
	"gitee.com/qqjtxc/project/devcloud/mcenter/apps/role"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// 创建策略
func (i *impl) CreatePolicy(ctx context.Context, in *policy.CreatePolicyRequest) (*policy.Policy, error) {
	// 构造一个 user 请求实例
	ins := policy.New(in)

	// 入库操作
	_, err := i.col.InsertOne(ctx, ins)
	if err != nil {
		return nil, err
	}

	return ins, nil
}

// 查询策略
func (i *impl) QueryPolicy(ctx context.Context, in *policy.QueryPolicyRequest) (*policy.PolicySet, error) {
	//初始化一个UserSet存放查询结果
	set := policy.NewPolicySet()

	// 构造查询条件
	filter := bson.M{}

	if in.UserId != "" {
		filter["user_id"] = in.UserId
	}
	if in.Namespace != "" {
		filter["namespace"] = in.Namespace
	}

	ops := &options.FindOptions{}
	ops.SetLimit(int64(in.Page.PageSize))
	ops.SetSkip(in.Page.ComputeOffset())

	cursor, err := i.col.Find(ctx, filter, ops)
	if err != nil {
		return nil, err
	}

	// 执行查询sql
	for cursor.Next(ctx) {
		ins := policy.NewDefaultPolicy()
		if err := cursor.Decode(ins); err != nil {
			return nil, err
		}
		// 对密码进行脱敏
		set.Add(ins)
	}

	// 判断是否关联查询
	if in.WithRole {
		req := role.NewQueryRoleRequest()
		req.AddRoleId(set.RoleIds()...)

		// 这里获取PageSize也可以直接使用Total?
		req.Page.PageSize = set.Len()
		rs, err := i.role.QueryRole(ctx, req)
		if err != nil {
			return nil, err
		}

		// 遍历角色列表
		for i := range rs.Items {
			r := rs.Items[i]
			set.SetRole(r)
		}
	}

	// 统计总数
	set.Total, err = i.col.CountDocuments(ctx, filter)
	if err != nil {
		return nil, err
	}
	return set, nil
}
