package impl_test

import (
	"testing"

	"gitee.com/qqjtxc/project/devcloud/mcenter/apps/policy"
)

func TestCreatePolicy(t *testing.T) {
	req := policy.NewCreatePolicyRequest()
	req.Namespace = "default"
	req.RoleId = "cj10j26v9mc87me9sl5g"
	req.UserId = "cj1220mv9mc9scksji60"
	ins, err := impl.CreatePolicy(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)
}

func TestQueryPolicy(t *testing.T) {
	req := policy.NewQueryPolicyRequest()
	req.WithRole = true
	ins, err := impl.QueryPolicy(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)
}
