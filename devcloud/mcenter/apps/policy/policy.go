package policy

import (
	"gitee.com/qqjtxc/project/devcloud/mcenter/apps/role"
	"gitee.com/qqjtxc/project/devcloud/mcenter/common/meta"
	request "github.com/infraboard/mcube/http/request"
)

func New(in *CreatePolicyRequest) *Policy {
	return &Policy{
		Meta: meta.NewMeta(),
		Spec: in,
	}
}

func NewCreatePolicyRequest() *CreatePolicyRequest {
	return &CreatePolicyRequest{}
}

// 初始化PolicySet对象
func NewPolicySet() *PolicySet {
	return &PolicySet{
		Items: []*Policy{},
	}
}

// 初始化一个角色对象
func NewDefaultPolicy() *Policy {
	return New(NewCreatePolicyRequest())
}

func (s *PolicySet) Add(ins *Policy) {
	s.Items = append(s.Items, ins)
}

func (s *PolicySet) GetRoles() (roles []*role.Role) {
	for i := range s.Items {
		item := s.Items[i]
		roles = append(roles, item.Role)
	}
	return
}

func NewQueryPolicyRequest() *QueryPolicyRequest {
	return &QueryPolicyRequest{
		Page: request.NewDefaultPageRequest(),
	}
}

func (s *PolicySet) RoleIds() (roleIds []string) {
	for i := range s.Items {
		item := s.Items[i]
		roleIds = append(roleIds, item.Spec.RoleId)
	}
	return
}

func (s *PolicySet) Len() uint64 {
	return uint64(len(s.Items))
}

func (s *PolicySet) SetRole(r *role.Role) {
	for i := range s.Items {
		item := s.Items[i]
		if item.Spec.RoleId == r.Meta.Id {
			item.Role = r
		}
	}
}
