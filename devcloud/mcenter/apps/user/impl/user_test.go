package impl_test

import (
	"testing"

	"gitee.com/qqjtxc/project/devcloud/mcenter/apps/user"
	"gitee.com/qqjtxc/project/devcloud/mcenter/test/tools"
)

func TestQueryUser(t *testing.T) {
	req := user.NewQueryUserRequest()
	req.Keywords = "test"
	set, err := impl.QueryUser(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tools.MustToJson(set))

}

func TestDescribeUser(t *testing.T) {
	req := user.NewDescribeUserRequest()
	req.DescribeBy = user.DESCRIBE_BY_USERNAME
	req.DescribeVaule = "admin"
	ins, err := impl.DescribeUser(ctx, req)
	if err != nil {
		t.Fatal(err)
	}

	t.Log(tools.MustToJson(ins))

	err = ins.CheckPassword("123456")
	if err != nil {
		t.Fatal(err)
	}
}
func TestCreateUser(t *testing.T) {
	req := user.NewCreateUserRequest()
	req.Username = "admin"
	req.Password = "123456"
	user, err := impl.CreateUser(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tools.MustToJson(user))
}
func TestUpdateUser(t *testing.T) {
	impl.UpdateUser(ctx, nil)
}
func TestDeleteUser(t *testing.T) {
	impl.DeleteUser(ctx, nil)
}
