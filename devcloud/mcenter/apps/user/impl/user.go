package impl

import (
	"context"

	"gitee.com/qqjtxc/project/devcloud/mcenter/apps/user"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// 查询用户列表
func (i *impl) QueryUser(ctx context.Context, in *user.QueryUserRequest) (*user.UserSet, error) {
	//初始化一个UserSet存放查询结果
	set := user.NewUserSet()

	// 构造查询条件
	filter := bson.M{}
	if in.Keywords != "" {
		// 完全匹配
		// filter["username"] = in.Keywords

		// 正则表达匹配
		filter["username"] = bson.M{"$regex": in.Keywords, "$options": "im"}
	}

	ops := &options.FindOptions{}
	ops.SetLimit(int64(in.Page.PageSize))
	ops.SetSkip(in.Page.ComputeOffset())

	cursor, err := i.col.Find(ctx, filter, ops)
	if err != nil {
		return nil, err
	}

	// 执行查询sql
	for cursor.Next(ctx) {
		ins := user.NewDefaultUser()
		if err := cursor.Decode(ins); err != nil {
			return nil, err
		}
		// 对密码进行脱敏
		ins.Desensi()
		set.Add(ins)
	}

	// 统计总数
	set.Total, err = i.col.CountDocuments(ctx, filter)
	if err != nil {
		return nil, err
	}
	return set, nil
}

// 查询用户详情
func (i *impl) DescribeUser(ctx context.Context, in *user.DescribeUserRequest) (*user.User, error) {
	filter := bson.M{}
	// 添加请求校验

	// 拼接请求条件
	switch in.DescribeBy {
	case user.DESCRIBE_BY_USER_ID:
		filter["_id"] = in.DescribeVaule
	case user.DESCRIBE_BY_USERNAME:
		filter["username"] = in.DescribeVaule
	}

	// 初始化获取请求结果
	ins := user.NewDefaultUser()

	err := i.col.FindOne(ctx, filter).Decode(ins)
	if err != nil {
		return nil, err
	}
	return ins, nil
}

// 用户的创建
func (i *impl) CreateUser(ctx context.Context, in *user.CreateUserRequest) (*user.User, error) {

	// 构造一个 user 请求实例
	ins, err := user.New(in)
	if err != nil {
		return nil, err
	}

	// 入库操作
	_, err = i.col.InsertOne(ctx, ins)
	if err != nil {
		return nil, err
	}

	return ins, nil
}

// 用户更新
func (i *impl) UpdateUser(ctx context.Context, in *user.UpdateUserRequest) (*user.User, error) {
	return nil, nil
}

// 用户删除
func (i *impl) DeleteUser(ctx context.Context, in *user.DeleteUserRequest) (*user.User, error) {
	return nil, nil
}
