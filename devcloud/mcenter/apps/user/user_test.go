package user_test

import (
	"testing"

	"gitee.com/qqjtxc/project/devcloud/mcenter/apps/user"
)

func TestNewCreateUserRequest(t *testing.T) {
	req := user.NewCreateUserRequest()
	req.Password = "123456"
	err := req.HashPassword()
	if err != nil {
		t.Fatal(err)
	}
	t.Log(req.Password)
}

func TestCheckPassword(t *testing.T) {
	const pass = "123456"
	req := user.NewCreateUserRequest()
	req.Password = pass
	req.Username = "test01"
	ins, _ := user.New(req)
	err := ins.CheckPassword(pass)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(pass)
}
