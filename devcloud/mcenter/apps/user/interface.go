package user

import (
	context "context"

	"gitee.com/qqjtxc/project/devcloud/mcenter/apps/domain"
	"gitee.com/qqjtxc/project/devcloud/mcenter/common/validate"
	"github.com/infraboard/mcube/http/request"
)

const (
	AppName = "users"
)

// 内部接口，模块内调用
type Service interface {
	// 用户的创建
	CreateUser(context.Context, *CreateUserRequest) (*User, error)
	// 用户更新
	UpdateUser(context.Context, *UpdateUserRequest) (*User, error)
	// 用户删除
	DeleteUser(context.Context, *DeleteUserRequest) (*User, error)
	// rpc只对外暴露了 查询用户列表（QueryUser） 和 查询用户详情（DescribeUser）
	RPCServer
}

func NewCreateUserRequest() *CreateUserRequest {
	return &CreateUserRequest{}
}

func NewDescribeUserRequest() *DescribeUserRequest {
	return &DescribeUserRequest{}
}

// 校验 CreateUserRequest 请求参数
func (req *CreateUserRequest) Validate() error {
	if req.Domain == "" {
		req.Domain = domain.DEFAULT_DOMAIN
	}
	return validate.V().Struct(req)
}

func NewQueryUserRequest() *QueryUserRequest {
	return &QueryUserRequest{
		Page: request.NewDefaultPageRequest(),
	}
}

func NewDescribeUserRequestByUsername(username string) *DescribeUserRequest {
	return &DescribeUserRequest{
		DescribeBy:    DESCRIBE_BY_USERNAME,
		DescribeVaule: username,
	}
}
