package user

import (
	"encoding/json"

	"gitee.com/qqjtxc/project/devcloud/mcenter/common/meta"
	"golang.org/x/crypto/bcrypt"
)

// 扩展protobuf 生成的结构体的方法,初始化一个创建用户请求参数
func New(req *CreateUserRequest) (*User, error) {
	// 先校验请求参数正确性
	if err := req.Validate(); err != nil {
		return nil, err
	}
	// 对密码进行hash操作
	if err := req.HashPassword(); err != nil {
		return nil, err
	}
	return &User{
		Spec: req,
		Meta: meta.NewMeta(),
	}, nil
}

// 初始化一个用户对象
func NewDefaultUser() *User {
	return &User{
		Meta: meta.NewMeta(),
		Spec: NewCreateUserRequest(),
	}
}

// 对密码进行hash
func (req *CreateUserRequest) HashPassword() error {
	// 专门给用户Password hash 的方法
	hp, err := bcrypt.GenerateFromPassword([]byte(req.Password), 10)
	if err != nil {
		return err
	}
	req.Password = string(hp)
	return nil
}

// 初始化UserSet对象
func NewUserSet() *UserSet {
	return &UserSet{
		Items: []*User{},
	}
}

func (s *UserSet) Add(ins *User) {
	s.Items = append(s.Items, ins)
}

// 自定义对象序列化方法，使用了匿名结构体嵌套
func (u *User) MarshalJSON() ([]byte, error) {
	return json.Marshal(struct {
		*meta.Meta
		*CreateUserRequest
	}{u.Meta, u.Spec})
}

// 用户密码的脱敏
func (i *User) Desensi() {
	i.Spec.Password = "***"
}

// 用户密码检查
func (i *User) CheckPassword(pass string) error {
	return bcrypt.CompareHashAndPassword([]byte(i.Spec.Password), []byte(pass))
}
