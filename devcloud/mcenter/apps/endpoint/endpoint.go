package endpoint

import (
	"fmt"
	"hash/fnv"

	"gitee.com/qqjtxc/project/devcloud/mcenter/common/logger"
	"gitee.com/qqjtxc/project/devcloud/mcenter/common/meta"
	"github.com/infraboard/mcube/http/request"
)

func NewEndpointSet() *EndpointSet {
	return &EndpointSet{
		Items: []*Endpoint{},
	}
}

func (s *EndpointSet) Add(item *Endpoint) {
	s.Items = append(s.Items, item)
}

func NewCreateEndpoingRequest() *CreateEndpoingRequest {
	return &CreateEndpoingRequest{}
}

func NewDefaultEndpoint() *Endpoint {
	return &Endpoint{
		Spec: NewCreateEndpoingRequest(),
		Meta: meta.NewMeta(),
	}
}

func NewRegistryRequest() *RegistryRequest {
	return &RegistryRequest{
		Items: []*CreateEndpoingRequest{},
	}
}

func NewQueryEndpointRequest() *QueryEndpointRequest {
	return &QueryEndpointRequest{
		Page: request.NewDefaultPageRequest(),
	}
}

func (r *CreateEndpoingRequest) UnitKey() string {
	return fmt.Sprintf("%s.%s.%s", r.ServiceId, r.Method, r.Path)
}

func (s *RegistryRequest) Add(item *CreateEndpoingRequest) {
	s.Items = append(s.Items, item)
}

func New(spec *CreateEndpoingRequest) *Endpoint {
	ep := &Endpoint{
		Meta: meta.NewMeta(),
		Spec: spec,
	}
	h := fnv.New32a()
	_, err := h.Write([]byte(ep.Spec.UnitKey()))
	if err != nil {
		logger.L().Debug().Msg("endpoint hash 失败了")
		panic(err)
	}
	ep.Meta.Id = fmt.Sprintf("%d", h.Sum32())
	return ep
}
