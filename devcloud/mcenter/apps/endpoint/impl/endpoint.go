package impl

import (
	"context"

	"gitee.com/qqjtxc/project/devcloud/mcenter/apps/endpoint"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func (i *impl) RegistryEndpoint(ctx context.Context, in *endpoint.RegistryRequest) (*endpoint.EndpointSet, error) {
	set := endpoint.NewEndpointSet()

	// 功能列表
	opt := &options.UpdateOptions{}
	opt.SetUpsert(true)
	for m := range in.Items {
		r := in.Items[m]
		ep := endpoint.New(r)
		_, err := i.col.UpdateOne(ctx,
			bson.M{"_id": ep.Meta.Id},
			bson.M{"$set": ep},
			opt,
		)
		if err != nil {
			return nil, err
		}
		set.Items = append(set.Items, ep)
	}
	return set, nil
}

func (i *impl) QueryEndpoint(ctx context.Context, in *endpoint.QueryEndpointRequest) (*endpoint.EndpointSet, error) {
	//初始化一个UserSet存放查询结果
	set := endpoint.NewEndpointSet()

	// 构造查询条件
	filter := bson.M{}

	ops := &options.FindOptions{}
	ops.SetLimit(int64(in.Page.PageSize))
	ops.SetSkip(in.Page.ComputeOffset())

	cursor, err := i.col.Find(ctx, filter, ops)
	if err != nil {
		return nil, err
	}

	// 执行查询sql
	for cursor.Next(ctx) {
		ins := endpoint.NewDefaultEndpoint()
		if err := cursor.Decode(ins); err != nil {
			return nil, err
		}
		set.Add(ins)
	}

	// 统计总数
	set.Total, err = i.col.CountDocuments(ctx, filter)
	if err != nil {
		return nil, err
	}
	return set, nil
}
