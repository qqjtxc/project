package impl_test

import (
	"testing"

	"gitee.com/qqjtxc/project/devcloud/mcenter/apps/endpoint"
)

func TestRegistryEndpoint(t *testing.T) {
	req := &endpoint.RegistryRequest{
		Items: []*endpoint.CreateEndpoingRequest{},
	}
	req.Items = append(req.Items, &endpoint.CreateEndpoingRequest{
		ServiceId: "service01",
		Method:    "GET",
		Path:      "/mcenter",
		Operation: "查询",
	})

	set, err := impl.RegistryEndpoint(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(set)
}

func TestQueryEndpoint(t *testing.T) {
	req := endpoint.NewQueryEndpointRequest()
	set, err := impl.QueryEndpoint(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(set)
}
