package impl

import (
	"context"

	"gitee.com/qqjtxc/project/devcloud/mcenter/apps/service"
	"go.mongodb.org/mongo-driver/bson"
)

// 服务的创建
func (i *impl) CreateService(ctx context.Context, in *service.CreateServiceRequest) (
	*service.Service, error) {

	// 构造一个 service 请求实例
	ins := service.New(in)

	// 入库操作
	_, err := i.col.InsertOne(ctx, ins)
	if err != nil {
		return nil, err
	}

	return ins, nil
}

// 服务的查询
func (i *impl) DescribeService(ctx context.Context, in *service.DescribeServiceRequest) (
	*service.Service, error) {
	filter := bson.M{}
	// 添加请求校验

	// 拼接请求条件
	switch in.DescribeBy {
	case service.DESCRIBE_BY_SERVICE_ID:
		filter["_id"] = in.DescribeVaule
	case service.DESCRIBE_BY_SERVICE_CRENDENTAIL_ID:
		// 这个是存在嵌套请求client_id 的
		filter["credentail.client_id"] = in.DescribeVaule
	}

	// 初始化获取请求结果
	ins := service.NewDefaultUser()

	err := i.col.FindOne(ctx, filter).Decode(ins)
	if err != nil {
		return nil, err
	}
	return ins, nil
}
