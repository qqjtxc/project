package impl_test

import (
	"testing"

	"gitee.com/qqjtxc/project/devcloud/mcenter/apps/service"
	"gitee.com/qqjtxc/project/devcloud/mcenter/test/tools"
)

func TestCreateService(t *testing.T) {
	req := &service.CreateServiceRequest{
		Domain:    "default",
		Namespace: "default",
		Name:      "mpass",
	}
	ins, err := impl.CreateService(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tools.MustToJson(ins))
}

func TestDescribeServiceByCrendentailId(t *testing.T) {
	req := &service.DescribeServiceRequest{
		DescribeBy:    service.DESCRIBE_BY_SERVICE_CRENDENTAIL_ID,
		DescribeVaule: "ci2nbh6v9mc31u5597p0",
	}
	ins, err := impl.DescribeService(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tools.MustToJson(ins))
}

func TestDescribeServiceById(t *testing.T) {
	req := &service.DescribeServiceRequest{
		DescribeBy:    service.DESCRIBE_BY_SERVICE_ID,
		DescribeVaule: "ci8qk9uv9mc5r1gdavh0",
	}
	ins, err := impl.DescribeService(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tools.MustToJson(ins))
}
