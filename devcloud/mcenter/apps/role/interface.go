package role

import "context"

const (
	AppName = "roles"
)

type Service interface {
	CreateRole(context.Context, *CreateRoleRequest) (*Role, error)
	RPCServer
}
