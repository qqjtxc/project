package role

import (
	meta "gitee.com/qqjtxc/project/devcloud/mcenter/common/meta"
	request "github.com/infraboard/mcube/http/request"
)

func NewCreateRoleRequest() *CreateRoleRequest {
	return &CreateRoleRequest{
		Features: []*Feature{},
	}
}

func (req *CreateRoleRequest) AddFeature(f *Feature) {
	req.Features = append(req.Features, f)
}

func New(req *CreateRoleRequest) *Role {

	return &Role{
		Spec: req,
		Meta: meta.NewMeta(),
	}
}

// 初始化RoleSet对象
func NewRoleSet() *RoleSet {
	return &RoleSet{
		Items: []*Role{},
	}
}

// 初始化一个角色对象
func NewDefaultRole() *Role {
	return New(NewCreateRoleRequest())
}

func (s *RoleSet) Add(ins *Role) {
	s.Items = append(s.Items, ins)
}

func NewQueryRoleRequest() *QueryRoleRequest {
	return &QueryRoleRequest{
		Page: request.NewDefaultPageRequest(),
	}
}

func (req *QueryRoleRequest) AddRoleId(ids ...string) {
	for i := range ids {
		req.RoleIds = append(req.RoleIds, ids[i])
	}
}

func (r *Role) HasFeatrue(serviceId, httpMethod, httpPath string) bool {
	for i := range r.Spec.Features {
		f := r.Spec.Features[i]
		if f.IsEqual(serviceId, httpMethod, httpPath) {
			return true
		}
	}
	return false
}

func (f *Feature) IsEqual(serviceId, httpMethod, httpPath string) bool {
	return f.ServiceId == serviceId &&
		f.HttpMethod == httpMethod &&
		f.HttpPath == httpPath
}
