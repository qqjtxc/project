package impl_test

import (
	"context"

	"gitee.com/qqjtxc/project/devcloud/mcenter/apps/role"
	"gitee.com/qqjtxc/project/devcloud/mcenter/test/tools"
	"github.com/infraboard/mcube/app"
)

var (
	// 声明需要测试的对象
	impl role.Service

	ctx = context.Background()
)

// 加载ioc, 需要加载测试用例的配置
func init() {
	// ioc 初始化
	tools.DevelopmentSetup()

	//从 ioc 里面取出需要测试的对象
	impl = app.GetInternalApp(role.AppName).(role.Service)
}
