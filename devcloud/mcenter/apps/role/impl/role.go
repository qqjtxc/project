package impl

import (
	"context"

	"gitee.com/qqjtxc/project/devcloud/mcenter/apps/role"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// 创建角色
func (i *impl) CreateRole(ctx context.Context, req *role.CreateRoleRequest) (*role.Role, error) {
	ins := role.New(req)

	_, err := i.col.InsertOne(ctx, ins)
	if err != nil {
		return nil, err
	}
	return ins, nil
}

// 查询角色列表
func (i *impl) QueryRole(ctx context.Context, req *role.QueryRoleRequest) (*role.RoleSet, error) {
	//初始化一个UserSet存放查询结果
	set := role.NewRoleSet()

	// 构造查询条件
	filter := bson.M{}

	if len(req.RoleIds) > 0 {
		// mongDb 中 IN的用法
		filter["_id"] = bson.M{"$in": req.RoleIds}
	}

	ops := &options.FindOptions{}
	ops.SetLimit(int64(req.Page.PageSize))
	ops.SetSkip(req.Page.ComputeOffset())

	cursor, err := i.col.Find(ctx, filter, ops)
	if err != nil {
		return nil, err
	}

	// 执行查询sql
	for cursor.Next(ctx) {
		ins := role.NewDefaultRole()
		if err := cursor.Decode(ins); err != nil {
			return nil, err
		}
		// 对密码进行脱敏
		set.Add(ins)
	}

	// 统计总数
	set.Total, err = i.col.CountDocuments(ctx, filter)
	if err != nil {
		return nil, err
	}
	return set, nil
}
