package impl_test

import (
	"testing"

	"gitee.com/qqjtxc/project/devcloud/mcenter/apps/role"
)

func TestCreateRole(t *testing.T) {
	req := role.NewCreateRoleRequest()
	req.Name = "admin"
	req.AddFeature(&role.Feature{
		ServiceId:  "service01",
		HttpMethod: "POST",
		HttpPath:   "/mcenter",
	})
	ins, err := impl.CreateRole(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)
}

func TestQueryRole(t *testing.T) {
	req := role.NewQueryRoleRequest()
	// req.AddRoleId("cj10j26v9mc87me9sl5g")
	set, err := impl.QueryRole(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(set)
}
