package token

import (
	"fmt"
	"time"
)

func NewToken() *Token {
	return &Token{
		IssueAt:          time.Now().Unix(), // 返回一个1970到现在的时间戳
		AccessExpiredAt:  600,
		RefreshExpiredAt: 600 * 4,
		Status:           &Status{},
		Meta:             map[string]string{},
	}
}

// 检查令牌是否可用
func (t *Token) CheckAvaliable() error {
	// 检查状态
	if t.Status.IsBlock {
		return fmt.Errorf(t.Status.BlockReason)
	}

	// 检查过期
	dura := time.Since(t.ExpiredTime())
	if dura >= 0 {
		return fmt.Errorf("令牌已经过期了")
	}

	return nil
}

func (t *Token) ExpiredTime() time.Time {
	dura := time.Duration(t.AccessExpiredAt * int64(time.Second))
	return time.Unix(t.IssueAt, 0).Add(dura)
}
