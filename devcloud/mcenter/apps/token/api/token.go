package api

import (
	"gitee.com/qqjtxc/project/devcloud/mcenter/apps/token"
	"github.com/emicklei/go-restful/v3"
	"github.com/infraboard/mcube/http/restful/response"
)

// 颁发Token
func (h *handler) IssueToken(r *restful.Request, w *restful.Response) {
	// 获取用户参数，强制 content type 来识别 body 里面内容的格式
	in := token.NewIssueTokenRequest()
	// 检查Accept标头并将内容读取到entityPointer中。
	err := r.ReadEntity(in)

	if err != nil {
		// 异常的处理： Accept,请求者希望 接收的数据格式
		// 抽象到公共库里面，mcube
		response.Failed(w, err)
		return
	}

	tk, err := h.service.IssueToken(r.Request.Context(), in)
	if err != nil {
		response.Failed(w, err)
	}

	w.WriteEntity(tk)

}
