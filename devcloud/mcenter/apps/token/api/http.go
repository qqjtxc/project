package api

import (
	"fmt"

	"github.com/emicklei/go-restful/v3"
	"github.com/infraboard/mcube/app"

	"gitee.com/qqjtxc/project/devcloud/mcenter/apps/token"
	restfulspec "github.com/emicklei/go-restful-openapi/v2"
)

// 定义一个实例对象初始化
var (
	h = &handler{}
)

// 定义一个实例对象
type handler struct {
	service token.Service
}

// 定义一个实例对象配置
func (h *handler) Config() error {
	// 这里的iot 直接使用的是第三方注册
	h.service = app.GetInternalApp(token.AppName).(token.Service)
	return nil
}

// 定义对象名称
func (h *handler) Name() string {
	return token.AppName
}

// 定义对象版本
func (h *handler) Version() string {
	return "v1"
}

// ws ：webServcie 对象
func (h *handler) Registry(ws *restful.WebService) {
	tags := []string{"登录"}
	fmt.Println(tags)
	// r.POST("/XXXX",HandleFunc)
	// Go Restful 路由配置是链式的：ws.Router(路由配置)
	// ws.POST("/") To(h.IssueToken) 都是路由装饰
	ws.Route(ws.POST("/").To(h.IssueToken).Doc("颁发令牌").
		Metadata(restfulspec.KeyOpenAPITags, tags).
		Reads(token.IssueTokenRequest{}).
		// 添加一个权限注释
		Metadata("auth", false).
		Metadata("resource", "令牌").
		Writes(token.Token{}).
		Returns(200, "OK", token.Token{}))
}

// 包初始化加载
func init() {
	// 这里的iot 直接使用的是第三方注册
	app.RegistryRESTfulApp(h)
}
