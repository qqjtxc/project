package token

import (
	context "context"
	"fmt"
)

const (
	AppName = "tokens"
)

type Service interface {
	// 令牌颁发: Restful
	IssueToken(context.Context, *IssueTokenRequest) (*Token, error)
	RPCServer
}

func NewValidateTokenRequest(ak string) *ValidateTokenRequest {
	return &ValidateTokenRequest{
		AccessToken: ak,
	}
}

func NewIssueTokenRequest() *IssueTokenRequest {
	return &IssueTokenRequest{}
}

// 校验请求参数
func (req *IssueTokenRequest) Validate() error {
	// 考虑到以后会介入钉钉、飞书等不通方案
	switch req.GrantType {
	case GRANT_TYPE_PASSWORD, GRANT_TYPE_LDAP:
		if req.Password == "" || req.Username == "" {
			return fmt.Errorf("用户名或密码缺失")
		}
	}
	return nil
}
