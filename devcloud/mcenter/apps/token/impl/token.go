package impl

import (
	"context"

	"gitee.com/qqjtxc/project/devcloud/mcenter/apps/token"
	"gitee.com/qqjtxc/project/devcloud/mcenter/apps/token/provider"
	"gitee.com/qqjtxc/project/devcloud/mcenter/common/logger"
	"github.com/infraboard/mcube/exception"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

// 令牌颁发: Restful
func (i *impl) IssueToken(ctx context.Context, in *token.IssueTokenRequest) (*token.Token, error) {
	// 参数校验
	if err := in.Validate(); err != nil {
		return nil, err
	}

	// 令牌颁发
	issuer := provider.Get(in.GrantType)
	ins, err := issuer.IssueToken(ctx, in)
	if err != nil {
		return nil, err
	}

	// 存储 到数据库中
	_, err = i.col.InsertOne(ctx, ins)
	if err != nil {
		return nil, err
	}

	return ins, nil
}

// 令牌的校验：Grpc
func (i *impl) ValidateToken(ctx context.Context, in *token.ValidateTokenRequest) (*token.Token, error) {
	tk := token.NewToken()
	// 查询数据库中的token
	err := i.col.FindOne(ctx, bson.M{"_id": in.AccessToken}).Decode(tk)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, exception.NewNotFound("%s not found", in.AccessToken)
			//return nil, err
		}
		return nil, err
	}

	//判断Token的状态
	err = tk.CheckAvaliable()
	logger.L().Debug().Msgf("检查token是否可用：", err)
	if err != nil {
		return nil, err
	}
	return tk, nil
}
