package impl_test

import (
	"testing"

	"gitee.com/qqjtxc/project/devcloud/mcenter/apps/token"
	"github.com/infraboard/mcube/exception"
)

func TestIssueToken(t *testing.T) {
	req := &token.IssueTokenRequest{
		Username: "test01",
		Password: "123456",
	}
	tk, err := impl.IssueToken(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tk)
}

func TestValidateToken(t *testing.T) {
	req := token.NewValidateTokenRequest("cj3ikrmv9mccmas00s50")
	tk, err := impl.ValidateToken(ctx, req)
	if err != nil {
		// 试用自己封装的err 打印错误信息，利用类型断言成自己封装的接口
		if s, ok := err.(exception.APIException); ok {
			t.Log(s.ToJson())
		}
		t.Fatal(err)
	}
	t.Log(tk)
}
