package provider

import "gitee.com/qqjtxc/project/devcloud/mcenter/apps/token"

// 初始化一个map对象存放 令牌
var (
	store = map[token.GRANT_TYPE]Issuer{}
)

func Registry(key token.GRANT_TYPE, value Issuer) {
	store[key] = value
}

// 定义一个返回值是自定义接口类型,实际返回的是实现这个接口的对象
func Get(key token.GRANT_TYPE) TokenIssuer {
	return store[key]
}

func Init() error {
	for _, v := range store {
		if err := v.Config(); err != nil {
			return nil
		}
	}
	return nil
}
