package password

import (
	"context"
	"fmt"

	"gitee.com/qqjtxc/project/devcloud/mcenter/apps/namespace"
	"gitee.com/qqjtxc/project/devcloud/mcenter/apps/token"
	"gitee.com/qqjtxc/project/devcloud/mcenter/apps/token/provider"
	"gitee.com/qqjtxc/project/devcloud/mcenter/apps/user"
	"gitee.com/qqjtxc/project/devcloud/mcenter/common/logger"
	"github.com/infraboard/mcube/app"
	"github.com/rs/xid"
)

type issuer struct {
	// 获取user信息
	user user.Service
}

func (i *issuer) IssueToken(ctx context.Context, in *token.IssueTokenRequest) (
	*token.Token, error) {
	// 查询该用户
	u, err := i.user.DescribeUser(ctx, user.NewDescribeUserRequestByUsername(in.Username))
	if err != nil {
		logger.L().Debug().Msg(err.Error())
		return nil, fmt.Errorf("用户名或密码错误")
	}

	// 检查密码
	err = u.CheckPassword(in.Password)
	if err != nil {
		logger.L().Debug().Msg(err.Error())
		return nil, fmt.Errorf("用户名或密码错误")
	}

	// 颁发令牌
	tk := token.NewToken()
	tk.AccessToken = xid.New().String()
	tk.RefreshToken = xid.New().String()
	tk.UserId = u.Meta.Id
	tk.Username = u.Spec.Username
	tk.Namespace = namespace.DEFAULT_NAMESPACE

	return tk, nil
}

func (i *issuer) Config() error {
	// 这里的app 是引用了第三方包注册 （"github.com/infraboard/mcube/app"）
	i.user = app.GetInternalApp(user.AppName).(user.Service)
	return nil
}

func init() {
	provider.Registry(token.GRANT_TYPE_PASSWORD, &issuer{})
}
