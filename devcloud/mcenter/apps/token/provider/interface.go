package provider

import (
	"context"

	"gitee.com/qqjtxc/project/devcloud/mcenter/apps/token"
)

// 定义一个令牌颁发接口
type Issuer interface {
	Config() error
	TokenIssuer
}

type TokenIssuer interface {
	IssueToken(context.Context, *token.IssueTokenRequest) (*token.Token, error)
}
