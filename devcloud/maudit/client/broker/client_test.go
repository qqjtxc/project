package broker_test

import (
	"context"
	"testing"
	"time"

	"gitee.com/qqjtxc/project/devcloud/maudit/apps/audit"
	"gitee.com/qqjtxc/project/devcloud/maudit/client/broker"
	"github.com/rs/xid"
)

var (
	ctx = context.Background()
)

func TestSendAuditLog(t *testing.T) {
	client := broker.NewClient("localhost:9092")
	defer client.Close()

	err := client.SendAuditLog(ctx, audit.AuditLog{
		Id:        xid.New().String(),
		Username:  "test",
		Time:      time.Now().Unix(),
		ServiceId: "mpaas 01",
		Operate:   "QueryCluster",
		Request:   "xxx",
	})

	if err != nil {
		t.Fatal(err)
	}
}
