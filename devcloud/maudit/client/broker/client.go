package broker

import (
	"context"

	"gitee.com/qqjtxc/project/devcloud/maudit/apps/audit"
	"github.com/segmentio/kafka-go"
)

func NewClient(brokers ...string) *Client {
	w := &kafka.Writer{
		Addr:                   kafka.TCP(brokers...),
		Topic:                  "topic-A",
		Balancer:               &kafka.LeastBytes{},
		AllowAutoTopicCreation: false,
	}
	return &Client{
		brokerAddress: brokers,
		writer:        w,
	}
}

type Client struct {
	brokerAddress []string
	writer        *kafka.Writer
}

func (c *Client) Close() error {
	return c.writer.Close()
}

// 发送审计日志
func (c *Client) SendAuditLog(ctx context.Context, al audit.AuditLog) error {
	// 构建一个kafka message
	m := kafka.Message{
		Key:   []byte(al.ServiceId),
		Value: al.ToJsonByte(),
	}

	return c.writer.WriteMessages(ctx, m)
}
