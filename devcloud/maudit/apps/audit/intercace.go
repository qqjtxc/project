package audit

import "context"

const (
	AppName = "audit"
)

// 审计日志接口
type Service interface {
	// 日志保存
	SaveAuditLog(context.Context, *AuditLog) (*AuditLog, error)
	// 日志查询
	QueryAuditLog(context.Context, *QueryAuditLogRequest) (*AuditLogSet, error)
}

type QueryAuditLogRequest struct {
}
