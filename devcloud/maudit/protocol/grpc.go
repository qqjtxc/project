package protocol

import (
	"net"

	"gitee.com/qqjtxc/project/devcloud/maudit/common/logger"
	"gitee.com/qqjtxc/project/devcloud/maudit/conf"
	"github.com/infraboard/mcube/app"
	"github.com/infraboard/mcube/grpc/middleware/recovery"
	"google.golang.org/grpc"
)

// NewGRPCServer todo
func NewGRPCService() *GRPCService {
	// 增加中间件避免报错程序崩了
	rc := recovery.NewInterceptor(recovery.NewZapRecoveryHandler())
	grpcServer := grpc.NewServer(grpc.ChainUnaryInterceptor(
		rc.UnaryServerInterceptor(),
	))

	return &GRPCService{
		svr: grpcServer,
		c:   conf.C(),
	}
}

// GRPCserver grpc服务
type GRPCService struct {
	svr *grpc.Server
	c   *conf.Config
}

// start 启动GRPC 服务
func (s *GRPCService) Start() {
	// 装载所有GRPC服务
	app.LoadGrpcApp(s.svr)

	// 启动HTTP服务
	lis, err := net.Listen("tcp", s.c.App.GRPC.Addr())
	if err != nil {
		logger.L().Debug().Msgf("listen grpc tcp conn error, %s", err)
		return
	}

	logger.L().Info().Msgf("GRPC 服务监听地址: %s", s.c.App.GRPC.Addr())
	if err := s.svr.Serve(lis); err != nil {
		if err == grpc.ErrServerStopped {
			logger.L().Info().Msg("service is stopped")
		}

		logger.L().Error().Msgf("start grpc service error, %s", err.Error())
		return
	}
}

// Stop 启动GRPC服务
func (s *GRPCService) Stop() error {
	s.svr.GracefulStop()
	return nil
}
