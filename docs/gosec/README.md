# 代码安全检查

源码地址[https://github.com/securego/gosec]

## 安装
Go 1.16+
```sh
go install github.com/securego/gosec/v2/cmd/gosec@latest
```

Go version < 1.16
```sh
go get -u github.com/securego/gosec/v2/cmd/gosec
```

可以把 $GOPATH 下的 bin 目录添加到 $PATH 中。这样你就可以像使用系统上的其他命令一样来使用 gosec 命令行工具（CLI）了。

## 使用方式
```sh
# 检查当前目录下所有包
gosec ./...

# 检查单个程序包
gosec $GOPATH/src/github.com/example/project

选项:
    -conf string          可选配置文件的路径
    -confidence string    以低于给定值的置信度筛选出问题 有效选项包括: low, medium, high (default "low")
    -exclude string       以逗号分隔的要排除的规则ID列表 请参阅"规则"列表
    -exclude-dir value    从扫描中排除目录 可以多次指定
    -fmt string           设置输出格式 有效选项包括: json, yaml, csv, junit-xml, html, sonarqube, golint or text (default "text")
    -include string       以逗号分隔的要包含的规则ID列表 请参阅"规则"列表
    -log string           将消息记录到文件而不是标准错误
    -no-fail              即使发现问题也不要使扫描失败
    -nosec                设置时忽略"#nosec"注释
    -nosec-tag string     为"#nosec"设置替代字符串 一些例子: #dontanalyze, #falsepositive
    -out string           设置结果的输出文件
    -quiet                仅在发现错误时显示输出
    -severity string      筛选出严重性低于给定值的问题 有效选项包括: low, medium, high (default "low")
    -sort                 按严重性对问题进行排序 (default true)
    -tags string          以逗号分隔的构建标记列表
    -tests                扫描测试文件
    -version              打印版本并退出 退出代码为0
```

## 结果解释
滚动屏幕你会看到不同颜色高亮的行：红色表示需要尽快查看的高优先级问题，黄色表示中优先级的问题。

## 自定义 gosec 扫描
gosec 是基于一系列的规则从 Go 源码中查找问题的。下面是它使用的完整的规则列表：
```sh
◈ G101：查找硬编码凭证
◈ G102：绑定到所有接口
◈ G103：审计 unsafe 块的使用
◈ G104：审计未检查的错误
◈ G106：审计 ssh.InsecureIgnoreHostKey 的使用
◈ G107: 提供给 HTTP 请求的 url 作为污点输入
◈ G108: /debug/pprof 上自动暴露的剖析端点
◈ G109: strconv.Atoi 转换到 int16 或 int32 时潜在的整数溢出
◈ G110: 潜在的通过解压炸弹实现的 DoS
◈ G201：SQL 查询构造使用格式字符串
◈ G202：SQL 查询构造使用字符串连接
◈ G203：在 HTML 模板中使用未转义的数据
◈ G204：审计命令执行情况
◈ G301：创建目录时文件权限分配不合理
◈ G302：使用 chmod 时文件权限分配不合理
◈ G303：使用可预测的路径创建临时文件
◈ G304：通过污点输入提供的文件路径
◈ G305：提取 zip/tar 文档时遍历文件
◈ G306: 写到新文件时文件权限分配不合理
◈ G307: 把返回错误的函数放到 defer 内
◈ G401：检测 DES、RC4、MD5 或 SHA1 的使用
◈ G402：查找错误的 TLS 连接设置
◈ G403：确保最小 RSA **长度为 2048 位
◈ G404：不安全的随机数源（rand）
◈ G501：导入黑名单列表：crypto/md5
◈ G502：导入黑名单列表：crypto/des
◈ G503：导入黑名单列表：crypto/rc4
◈ G504：导入黑名单列表：net/http/cgi
◈ G505：导入黑名单列表：crypto/sha1
◈ G601: 在 range 语句中使用隐式的元素别名
```
+ 排除指定的测试
```sh
$ gosec -exclude=G104 ./...
$ gosec -exclude=G104,G101 ./...
```

+ 运行指定的检查
```sh
$ gosec -include=G201,G202 ./...
```

+ 扫描测试文件
```sh
gosec -tests ./...
```

+ 修改输出格式
```sh
$ gosec -fmt=json -out=results.json ./...
```
