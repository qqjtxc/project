package main

import (
	"context"
	"fmt"
	"log"

	"gitee.com/qqjtxc/project/minio/apps/client/config"
	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
)

// 初始化一个客户端连接
func MinioCli() (*minio.Client, error) {
	minioMsg := config.NewMinioMsg()
	// 下面的 *** 为自己的minio环境信息
	minioMsg.AccessKeyID = "***"
	minioMsg.Endpoint = "***"
	minioMsg.SecretAccessKey = "***"
	minioMsg.UseSSL = false
	MinioClient, err := minio.New(minioMsg.Endpoint, &minio.Options{
		Creds:  credentials.NewStaticV4(minioMsg.AccessKeyID, minioMsg.SecretAccessKey, ""),
		Secure: minioMsg.UseSSL,
	})
	if err != nil {
		return nil, err
	}
	return MinioClient, nil
}

type MinioRequest struct {
	// bucket 的名字
	BucketName string
	// 上传的对象
	ObjectName string
	// 本地上传的路径
	FilePath string
	// 上传的文件类型
	ContentType string
}

func NewMinioRequest() *MinioRequest {
	return &MinioRequest{}
}

var ctx = context.Background()

func main() {
	MinioClient, _ := MinioCli()

	req := NewMinioRequest()
	req.BucketName = "pbu-public"
	req.ObjectName = "test.zip"
	req.FilePath = "/tmp/test.zip"
	req.ContentType = "application/zip"
	info, err := MinioClient.FPutObject(ctx, req.BucketName, req.ObjectName, req.FilePath,
		minio.PutObjectOptions{ContentType: req.ContentType})
	if err != nil {
		fmt.Println(err)
	}

	log.Printf("Successfully uploaded %s of size %d\n", req.ObjectName, info.Size)

}
