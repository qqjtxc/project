package client_test

import (
	"testing"

	"gitee.com/qqjtxc/project/minio/apps/client"
)

func TestMinioCli(t *testing.T) {
	_, err := client.MinioCli()
	if err != nil {
		t.Fatal(err)
	}
	t.Log("ok")
}
