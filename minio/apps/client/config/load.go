package config

import (
	"github.com/BurntSushi/toml"
)

// 从toml文件中读取配置信息
func LoadConfigFromToml(filepath string) (*MinioMsg, error) {
	Msg := NewConfig()
	_, err := toml.DecodeFile(filepath, Msg)
	if err != nil {
		return nil, err
	}
	return Msg.MinioMsg, nil
}

// 从文件中读取配置信息
