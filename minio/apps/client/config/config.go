package config

// 定义一个全局配置文件路径
var EtcFilePath = "/Users/xuchi/go/project/project/minio/apps/client/etc/config.toml"

// 保存minio的连接信息
type Config struct {
	MinioMsg *MinioMsg `toml:"minio"`
}

// 初始化一个默认配置
func NewConfig() *Config {
	return &Config{
		MinioMsg: NewMinioMsg(),
	}
}

// 连接minio的配置信息
type MinioMsg struct {
	Endpoint        string `toml:"endpoint"`
	AccessKeyID     string `toml:"accessKeyID"`
	SecretAccessKey string `toml:"secretAccessKey"`
	UseSSL          bool   `toml:"useSSL"`
}

// minio连接信息初始化
func NewMinioMsg() *MinioMsg {
	return &MinioMsg{}
}
