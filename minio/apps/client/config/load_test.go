package config_test

import (
	"testing"

	"gitee.com/qqjtxc/project/minio/apps/client/config"
)

func TestLoadConfigFromToml(t *testing.T) {

	msg, err := config.LoadConfigFromToml(config.EtcFilePath)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(msg)
}
