package client

import (
	"gitee.com/qqjtxc/project/minio/apps/client/config"
	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
)

// 初始化一个客户端连接
func MinioCli() (*minio.Client, error) {

	// 获取minio配置信息
	minioMsg, err := config.LoadConfigFromToml(config.EtcFilePath)
	if err != nil {
		return nil, err
	}
	// 建立连接
	minioClient, err := minio.New(minioMsg.Endpoint, &minio.Options{
		Creds:  credentials.NewStaticV4(minioMsg.AccessKeyID, minioMsg.SecretAccessKey, ""),
		Secure: minioMsg.UseSSL,
	})
	if err != nil {
		return nil, err
	}
	return minioClient, nil
}
