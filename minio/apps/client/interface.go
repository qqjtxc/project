package client

import (
	"context"

	"github.com/minio/minio-go/v7"
)

// 定义一个全局minio 客户端连接
type Service interface {
	// 文件上传
	PutObject(context.Context, *MinioRequest) (*MinioResponse, error)
}

// 定义一个请求实例
type MinioRequest struct {
	// bucket 的名字
	BucketName string
	// 上传的对象
	ObjectName string
	// 本地上传的路径
	FilePath string
	// 上传的文件类型
	ContentType string
}

func NewMinioRequest() *MinioRequest {
	return &MinioRequest{}
}

// 定义一个响应实例
type MinioResponse struct {
	MiUploadInfo minio.UploadInfo
}

func NewMinioResponse() *MinioResponse {
	return &MinioResponse{}
}
