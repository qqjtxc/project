package impl_test

import (
	"context"

	"gitee.com/qqjtxc/project/minio/apps/client"
	"gitee.com/qqjtxc/project/minio/apps/client/impl"
)

var (
	svc = &impl.Impl{}
	ctx = context.Background()
)

func init() {
	minioClient, err := client.MinioCli()
	if err != nil {
		panic(err)
	}
	svc.MinioClient = minioClient
}
