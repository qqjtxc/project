package impl

import "github.com/minio/minio-go/v7"

type Impl struct {
	MinioClient *minio.Client
}
