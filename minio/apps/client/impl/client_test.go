package impl_test

import (
	"testing"

	"gitee.com/qqjtxc/project/minio/apps/client"
)

func TestUpload(t *testing.T) {

	req := client.NewMinioRequest()
	req.BucketName = "pbu-public"
	req.ObjectName = "test.zip"
	req.FilePath = "/tmp/test.zip"
	req.ContentType = "application/zip"

	res, err := svc.PutObject(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(res.MiUploadInfo.Size)
}
