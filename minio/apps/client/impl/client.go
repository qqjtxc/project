package impl

import (
	"context"

	"gitee.com/qqjtxc/project/minio/apps/client"
	"github.com/minio/minio-go/v7"
)

func (i *Impl) PutObject(ctx context.Context, req *client.MinioRequest) (*client.MinioResponse, error) {

	// 上传文件至minio
	info, err := i.MinioClient.FPutObject(ctx, req.BucketName, req.ObjectName, req.FilePath,
		minio.PutObjectOptions{ContentType: req.ContentType})
	if err != nil {
		return nil, err
	}

	Res := client.NewMinioResponse()
	Res.MiUploadInfo = info
	return Res, nil
}
